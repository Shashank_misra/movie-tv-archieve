package com.hack.lucifer.movies.Serials;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.hack.lucifer.movies.Databases.Serial_DB_Helper;

public class SuggestionContentProvider extends ContentProvider {

    SQLiteDatabase db;

    @Override
    public boolean onCreate() {

        Serial_DB_Helper helper = new Serial_DB_Helper(getContext());
        db = helper.getReadableDatabase();
        return db != null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projections, String selection, String[] selectionArgs, String sortOrder) {


        if (selectionArgs[0].length() > 0) {
            Cursor cursor = db.rawQuery("SELECT " +
                    Serial_DB_Helper.SERIAL + " as " + SearchManager.SUGGEST_COLUMN_TEXT_1 +
                    ", " + Serial_DB_Helper.SERIAL + " as " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID +
                    ", _ID from " + Serial_DB_Helper.TABLE_SERIALS
                    + " where " + Serial_DB_Helper.SERIAL + " like '%" + selectionArgs[0] + "%' group by " +
                    SearchManager.SUGGEST_COLUMN_TEXT_1, new String[]{});
            return cursor;
        } else
            return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
