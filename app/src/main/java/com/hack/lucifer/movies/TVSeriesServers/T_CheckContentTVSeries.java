package com.hack.lucifer.movies.TVSeriesServers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.MainActivity;
import com.hack.lucifer.movies.SplashActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

import javax.net.ssl.HttpsURLConnection;

public class T_CheckContentTVSeries extends AsyncTask<String, String, String[]> {

    SplashActivity callingClass;

    public T_CheckContentTVSeries(Activity activity) {
        this.callingClass = (SplashActivity) activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callingClass.setUpdateMessage("Checking TVServer Cache");
    }

    @Override
    protected String[] doInBackground(String... strings) {

        int resCode;
        InputStream in;
        int error;
        String result;
        try {
            URL url = new URL(Constants.BLOBID_TVSERIES_VAHID);
            URLConnection urlConn = url.openConnection();

            HttpsURLConnection httpsConn = (HttpsURLConnection) urlConn;
            httpsConn.setAllowUserInteraction(false);
            httpsConn.setInstanceFollowRedirects(true);
            httpsConn.setRequestMethod("GET");
            httpsConn.connect();
            resCode = httpsConn.getResponseCode();
            Log.d("msg", "response: " + resCode);
            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpsConn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        in, "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                in.close();
                result = sb.toString();
                Log.d("msg", result);

                JSONObject response = new JSONObject(result);
                Log.d("msg", "got response");

                String serverName = response.getString("serverName");
                JSONArray seriesArray = response.getJSONArray("list");
                int countVideos = response.optInt("countVideos");

                SharedPreferences preferences = callingClass.
                        getSharedPreferences(AppController.packageName,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("count", countVideos);
                editor.commit();
                Log.d("msg", countVideos + "");

                Log.d("msg", "movie length: " + seriesArray.length());

                if (seriesArray.length() == 0) {

                    Log.d("msg", "fetchContent started");
                    publishProgress("Cache not found!");
                    return new String[]{serverName};

                } else {

                    publishProgress("Cache found! Updating TV list.");
                    for (int k = 0; k < seriesArray.length(); k++) {
                        JSONObject m = seriesArray.getJSONObject(k);
                        T_FetchContentVahid.Serial serial = Utils.parseTVSerialBlob(m);
                        if (callingClass.serialData == null)
                            callingClass.serialData = new ArrayList<>();
                        callingClass.serialData.add(serial);
                    }
                    Collections.sort(callingClass.serialData);
                }
            } else {
                error = resCode;
                Log.d("msg", "error code: " + error);
                publishProgress("_");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            publishProgress("_");
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {

        super.onProgressUpdate(values);
        if (values[0].equals("_")) {
            callingClass.setUpdateMessage("Exception at CH_TV");
            Toast.makeText(callingClass, "CRASHED", Toast.LENGTH_SHORT).show();
        } else {
            callingClass.setUpdateMessage(values[0]);
        }
    }

    @Override
    protected void onPostExecute(String... s) {
        super.onPostExecute(s);

        if (s != null) {
            T_FetchContentVahid fetch = new T_FetchContentVahid(callingClass, Constants.BASE_URL_VAHID);
            fetch.execute();
        } else {

            boolean response = callingClass.writeSerializedObject(callingClass.serialData, MainActivity.serialFilePath);
            Log.d("msg", "writing serial serialized Object response: " + response);

            Intent intent = new Intent(callingClass, MainActivity.class);
            MainActivity.globalReadyMovies = callingClass.movieDataFinal;
            MainActivity.globalReadySerials = callingClass.serialData;
            MainActivity.databaseIsUpdated = false;
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("movieDataFinal", callingClass.movieDataFinal);
//            bundle.putSerializable("serialData", callingClass.serialData);
//            intent.putExtras(bundle);
            callingClass.startActivity(intent);
            callingClass.finish();
        }
    }
}