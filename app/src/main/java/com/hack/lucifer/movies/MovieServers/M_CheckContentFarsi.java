package com.hack.lucifer.movies.MovieServers;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.Movies.FinishedMovie;
import com.hack.lucifer.movies.SplashActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

import javax.net.ssl.HttpsURLConnection;

public class M_CheckContentFarsi extends AsyncTask<String, String, String[]> {

    SplashActivity callingClass;

    public M_CheckContentFarsi(Activity activity) {
        this.callingClass = (SplashActivity) activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callingClass.setUpdateMessage("Checking Server 1 Cache");
    }

    @Override
    protected String[] doInBackground(String... strings) {

        int resCode;
        InputStream in;
        int error = 0;
        String result;
        try {
            URL url = new URL(Constants.BLOBID_FARSI);
            URLConnection urlConn = url.openConnection();

            HttpsURLConnection httpsConn = (HttpsURLConnection) urlConn;
            httpsConn.setAllowUserInteraction(false);
            httpsConn.setInstanceFollowRedirects(true);
            httpsConn.setRequestMethod("GET");
            httpsConn.connect();
            resCode = httpsConn.getResponseCode();
            Log.d("msg", "response: " + resCode);
            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpsConn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        in, "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                in.close();
                result = sb.toString();
                JSONObject response = new JSONObject(result);

                String serverName = response.getString("serverName");
                String serverURL = response.getString("serverURL");
                JSONArray moviArray = response.getJSONArray("movieList");
                Log.d("msg", "movie length: " + moviArray.length());
                if (moviArray.length() == 0) {

                    Log.d("msg", "fetchContent started");
                    publishProgress("Server Cache Unavailable!");
                    return new String[]{serverURL, serverName};
                } else {

                    publishProgress("Server Cache found! Updating List");
                    for (int k = 0; k < moviArray.length(); k++) {
                        JSONObject m = moviArray.getJSONObject(k);
                        FinishedMovie fm = Utils.parseMovieBlob(m);
                        if (callingClass.movieDataFinal == null)
                            callingClass.movieDataFinal = new ArrayList<>();
                        callingClass.movieDataFinal.add(fm);
                    }
                    Collections.sort(callingClass.movieDataFinal);
                }
            } else {
                error = resCode;
                publishProgress("_");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            publishProgress("_");
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        if (values.equals("_")) {
            Toast.makeText(callingClass, "Servers Not Responding", Toast.LENGTH_SHORT).show();
        } else {
            callingClass.setUpdateMessage(values[0]);
        }
    }

    @Override
    protected void onPostExecute(String... s) {
        super.onPostExecute(s);

        if (s != null) {
            M_FetchContentFarsi fetch = new M_FetchContentFarsi(callingClass, s[0], s[1]);
            fetch.execute();
        } else {
//            MovieFragment.movieFragment.listAdapter.notifyDataSetChanged();
            M_CheckContent1327 check = new M_CheckContent1327(callingClass);
            check.execute();
        }
    }
}
