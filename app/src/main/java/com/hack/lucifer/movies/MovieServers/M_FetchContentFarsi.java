package com.hack.lucifer.movies.MovieServers;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.Movies.FinishedMovie;
import com.hack.lucifer.movies.MainActivity;
import com.hack.lucifer.movies.SplashActivity;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class M_FetchContentFarsi extends AsyncTask<Void, String, ArrayList<FinishedMovie>> {

    String queryURL, serverName;
    SplashActivity callingClass;

    M_FetchContentFarsi(Activity activity, String serverURL, String servername) {

        callingClass = (SplashActivity) activity;
        queryURL = serverURL;
        this.serverName = servername;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callingClass.setUpdateMessage("Connecting to Server 1");
    }

    @Override
    protected ArrayList<FinishedMovie> doInBackground(Void... voids) {

        /*
        Fetch All Content Links
         */
        Document document = null;
        try {
            document = Jsoup.connect(queryURL).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert document != null;
        Elements table = document.select("table");
        Elements rows = table.select(":not(thead) tr");

        final ArrayList<String> urls = new ArrayList<>();
        for (int i = 2; i < rows.size() - 2; i++) {
            Element row = rows.get(i);
            Elements row_attr = row.select("td");
            Log.d("msg", Constants.BASE_URL_FARSI + row_attr.get(0).text());
            urls.add(Constants.BASE_URL_FARSI + row_attr.get(0).text());

            publishProgress("Found " + urls.size() + " links");
        }

        /*
        Fetching all the download links
         */
        publishProgress("S1: Finding download links");
        int movieCount = 0;
        final ArrayList<MainActivity.rawMovie> rawMovies = new ArrayList<>();

        for (String u : urls) {
            try {
                document = Jsoup.connect(u).get();
            } catch (IOException e) {
//                    e.printStackTrace();
            }
            table = document.select("table");
            rows = table.select(":not(thead) tr");

            for (int i = 2; i < rows.size(); i++) {
                Element row = rows.get(i);
                Elements row_attr = row.select("td");

                Elements temp = row_attr.get(0).select("a[href]");
                String url = temp.attr("abs:href");
                String nameIn = row_attr.get(0).text();
                String size = row_attr.get(1).text();

                String part[] = u.split("/");
//                    Log.d("msg",  "\nurl: " + url + "\nnameIn: " + nameIn + "\nsize: " + size + "\nyr: " + part[part.length - 1]);
                rawMovies.add(new MainActivity.rawMovie(nameIn, url, size, part[part.length - 1]));
                movieCount++;
                publishProgress("Downloadable: " + movieCount);
            }
        }
        callingClass.movieDataRaw.addAll(rawMovies);
        publishProgress("O: Fetching Metadata");

        /*
        Fetching Metadata for all movies
         */
        final int[] count = {0};
        int lCount = 1;
        Log.d("m_name", callingClass.movieDataRaw.size() + " movies to go for metadata");
        int currReadyListSize = callingClass.movieDataFinal.size();
        for (final MainActivity.rawMovie rm : callingClass.movieDataRaw) {
            try {
                String query_url = Constants.OMDB_BASE_URL +
                        "t=" + rm.name;
                if (rm.yr != null)
                    query_url += "&y=" + rm.yr;
                query_url += "&plot=full&r=json";
                Log.d("msg", lCount++ + ". " + rm.name);

                JSONObject response = com.hack.lucifer.movies.Utils.getJSONObjectFromUrl(query_url);
                Log.d("msg", "returned with: " + response);
                if (response != null) {
                    FinishedMovie result = Utils.parseOMDBDataForMovie(response.toString(), rm.url, rm.size, rm.name);
                    if (result != null) {
                        callingClass.movieDataFinal.add(result);
                        count[0]++;
                    }
                    publishProgress("O: Metadata found: " + count[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        publishProgress("Caching data to local Server");
//            Collections.sort(globalReadyMovies);
        JsonObjectRequest putOnServer = new JsonObjectRequest(Request.Method.PUT,
                Constants.BLOBID_FARSI,
                Utils.getTemplateJsonForMovie(callingClass.movieDataFinal, new String[]{serverName}, new String[]{queryURL}, currReadyListSize),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callingClass.setUpdateMessage("Saved To local Server");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callingClass.setUpdateMessage("Failed To Update to local Server");
            }
        });
        AppController.getInstance().addToRequestQueue(putOnServer);
        return callingClass.movieDataFinal;
    }

    @Override
    protected void onProgressUpdate(String... values) {

        if (!values[0].equals("_"))
            callingClass.setUpdateMessage(values[0]);
        else
            callingClass.setUpdateMessage("Exception in FETCH_CO_FSI");
    }

    @Override
    protected void onPostExecute(ArrayList<FinishedMovie> finishedMovies) {
        super.onPostExecute(finishedMovies);
//        if (MovieFragment.movieFragment != null) {
//            MovieFragment.movieFragment.listAdapter.notifyDataSetChanged();
//        }

        M_CheckContent1327 check = new M_CheckContent1327(callingClass);
        check.execute();
    }
}
