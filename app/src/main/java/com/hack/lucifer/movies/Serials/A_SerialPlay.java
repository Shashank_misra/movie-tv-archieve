package com.hack.lucifer.movies.Serials;


import android.app.Activity;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.devbrackets.android.exomedia.EMVideoView;
import com.devbrackets.android.exomedia.listener.ExoPlayerListener;
import com.hack.lucifer.movies.R;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class A_SerialPlay extends Activity {

    int videoIndex = 0;
    Vibrator vibrator;

    private EMVideoView mVideoView;
    private SeekBar seekBar;
    private ProgressBar progressBar;
    private TextView title, videoCurrentTime, videoDuration;
    private ImageView playPause;
    private RelativeLayout c1;
    private RelativeLayout timer;
    private HorizontalScrollView episodeHS;

    private boolean isVideoComplete;
    private boolean listVisible = false;
    private boolean everyThingHidden = false;

    private Runnable onEverySecond = new Runnable() {
        {

        }

        @Override
        public void run() {

            if (seekBar != null) {

                if (mVideoView.isPlaying())
                    seekBar.setProgress((int) mVideoView.getCurrentPosition());

                seekBar.setSecondaryProgress((mVideoView.getBufferPercentage() * seekBar.getMax()) / 100);
                videoCurrentTime.setText(String.format(" %02d:%02d ",
                        TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()),
                        TimeUnit.MILLISECONDS.toSeconds(mVideoView.getCurrentPosition()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()))));
            }

            if (mVideoView != null) {
                seekBar.postDelayed(onEverySecond, 1000);
            }

            if (mVideoView != null) {
                if (mVideoView.getCurrentPosition() >= mVideoView.getDuration()) {
                    isVideoComplete = true;
                    progressBar.setVisibility(View.GONE);
                    if (videoIndex < seasonPlaying.episodes.size() - 1) {
                        videoIndex++;
                        playEpisodes(seasonPlaying);
                    }

                    mVideoView.release();
                }
            }
        }
    };

    private void hideControls() {

        if (mVideoView.isPlaying()) {
            c1.animate().translationY(-1 * (c1.getHeight() + 2 * title.getPaddingTop()));
            timer.animate().translationY(timer.getHeight());
            if (episodeHS.getVisibility() == View.VISIBLE) {
                episodeHS.animate().translationY(episodeHS.getWidth() + timer.getHeight());
            }
            playPause.setVisibility(View.GONE);
            everyThingHidden = true;
        }
    }

    private void showControls() {

        c1.animate().translationY(0);
        timer.animate().translationY(0);
        if (episodeHS.getVisibility() == View.VISIBLE) {
            episodeHS.animate().translationY(0);
        }
        playPause.setVisibility(View.VISIBLE);
        if (mVideoView.isPlaying()) {
            playPause.setImageResource(R.drawable.ic_pause_white_100dp);
        } else {
            playPause.setImageResource(R.drawable.ic_play_arrow_white_100dp);
        }

        everyThingHidden = false;
    }

    String imageUrl = "";

    T_FetchContentVahid.Serial.Season seasonPlaying;
    T_FetchContentVahid.Serial serialRunning;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("msg", "in OnCreate()");
        setContentView(R.layout.activity_play_series_c);

        serialRunning = (T_FetchContentVahid.Serial) getIntent().getSerializableExtra("serial");
        int position = getIntent().getIntExtra("seasonNumber", 0);
        seasonPlaying = serialRunning.seasons.get(position);
        imageUrl = getIntent().getStringExtra("image");

        initialize();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Log.d("Season Data: ", "" + seasonPlaying.episodes.size());
        title.setText(serialRunning.name + "\n" +
                seasonPlaying.getName() + " " +
                seasonPlaying.episodes.get(videoIndex).getDisplayName() + "/" + seasonPlaying.episodes.size());
        playEpisodes(seasonPlaying);
    }

    private void initialize() {

        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        c1 = (RelativeLayout) findViewById(R.id.c1);
        timer = (RelativeLayout) findViewById(R.id.timerDisplay);
        episodeHS = (HorizontalScrollView) findViewById(R.id.episodesHS);
        playPause = (ImageView) findViewById(R.id.playPause);
        mVideoView = (EMVideoView) findViewById(R.id.player);
        ImageView listToggle = (ImageView) findViewById(R.id.toggleList);
        title = (TextView) findViewById(R.id.title);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        videoCurrentTime = (TextView) findViewById(R.id.videoCurrentTime);
        videoDuration = (TextView) findViewById(R.id.videoDuration);

//        int[][] states = new int[][]{
//                new int[]{android.R.attr.state_enabled}, // enabled
//                new int[]{android.R.attr.state_pressed}  // pressed
//        };
//
//        int[] colors = new int[]{
//                getResources().getColor(R.color.colorPrimary),
//                Color.YELLOW
//        };
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            seekBar.setSecondaryProgressTintList(new ColorStateList(states, colors));
//        }
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if (fromUser) {
                    if (mVideoView != null) {
                        mVideoView.seekTo(progress);
                    }
                }
            }
        });

        listToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vibrator.vibrate(10);
                if (!listVisible)
                    episodeHS.setVisibility(View.VISIBLE);
                else
                    episodeHS.setVisibility(View.GONE);
                listVisible = !listVisible;
            }
        });

        mVideoView.addExoPlayerListener(new ExoPlayerListener() {
            @Override
            public void onStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady) {
                    if (playbackState == 4) {
                        progressBar.setVisibility(View.GONE);
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onVideoSizeChanged(int width, int height, int unAppliedRotationDegrees, float pixelWidthHeightRatio) {

            }
        });
        mVideoView.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideControls();
            }
        }, 5000);
        mVideoView.setOnClickListener(new View.OnClickListener() {
            {

            }

            @Override
            public void onClick(View view) {
                if (everyThingHidden)
                    showControls();
                else
                    hideControls();
            }
        });

        playPause.setOnClickListener(new View.OnClickListener() {
            {

            }

            @Override
            public void onClick(View v) {

                vibrator.vibrate(10);
                if (mVideoView != null) {
                    if (mVideoView.isPlaying()) {
                        mVideoView.pause();
                        playPause.setImageResource(R.drawable.ic_play_arrow_white_100dp);
                        progressBar.setVisibility(View.GONE);
                    } else {
                        mVideoView.start();
                        playPause.setImageResource(R.drawable.ic_pause_white_100dp);
                        mVideoView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideControls();
                            }
                        }, 5000);
                    }
                    if (isVideoComplete) {
                        if (mVideoView != null) {
                            isVideoComplete = false;
                            mVideoView.seekTo(0);
                            mVideoView.setVideoURI(Uri.parse(seasonPlaying.episodes.get(videoIndex).url));
                            mVideoView.start();
                            mVideoView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    hideControls();
                                }
                            }, 5000);
                        }
                    }
                }
            }
        });

        LinearLayout container = (LinearLayout) findViewById(R.id.epiListContainer);
        final View epiViewList[] = new View[seasonPlaying.episodes.size()];
        int counter = 0;
        boolean selectFirst = true;
        for (T_FetchContentVahid.Serial.Season.Episode e : seasonPlaying.episodes) {

            View epi = View.inflate(this, R.layout.ele_episodelist, null);
            final TextView epiname = (TextView) epi.findViewById(R.id.name);
            epiname.setText(e.episodeNumber + "");

            if (selectFirst) {
                epiname.setTextColor(Color.rgb(21, 21, 21));
                epiname.setBackgroundResource(R.drawable.circular_textview_selected);
                selectFirst = false;
            }

            final int finalCounter = counter;
            final String name = e.getDisplayName();
            epi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    vibrator.vibrate(10);
                    if (videoIndex != finalCounter) {
                        videoIndex = finalCounter;
                        mVideoView.removeCallbacks(onEverySecond);
                        playEpisodes(seasonPlaying);
                        title.setText(serialRunning.name + "\n" +
                                seasonPlaying.getName() + " " + name +
                                "/" + seasonPlaying.episodes.size());

                        epiname.setTextColor(Color.rgb(21, 21, 21));
                        epiname.setBackgroundResource(R.drawable.circular_textview_selected);

                        for (int i = 0; i < seasonPlaying.episodes.size(); i++) {
                            TextView tv = (TextView) epiViewList[i].findViewById(R.id.name);
                            if (i != finalCounter) {
                                tv.setBackgroundResource(R.drawable.circular_textview_normal);
                                tv.setTextColor(Color.WHITE);
                            }
                        }
                    }
                }
            });

            if (counter == 0)
                epiname.setTextColor(getResources().getColor(R.color.colorPrimary));

            container.addView(epi);
            epiViewList[counter++] = epi;
        }
    }

    private void playEpisodes(T_FetchContentVahid.Serial.Season seasonPlaying) {

        Log.d("msg", seasonPlaying.episodes.get(videoIndex).url);
        mVideoView.seekTo(0);

        String videoID = seasonPlaying.episodes.get(videoIndex).getID();
        DBManager dbManager = new DBManager(this);
        String path = dbManager.getOfflineLocation(videoID);
        Log.d("msg", path + "");

        if (path != null && path.length() > 0) {
            File file = new File(path);
            if (file.exists())
                mVideoView.setVideoURI(Uri.parse(path));
            else {
                dbManager.putNullToStorePath(videoID);

            }
        } else
            mVideoView.setVideoURI(Uri.parse(seasonPlaying.episodes.get(videoIndex).url));

        mVideoView.start();
        progressBar.setVisibility(View.VISIBLE);

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            {

            }

            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {

                seekBar.setMax((int) mVideoView.getDuration());
                progressBar.setVisibility(View.GONE);
                videoDuration.setText(String.format(" %02d:%02d ",
                        TimeUnit.MILLISECONDS.toMinutes(mVideoView.getDuration()),
                        TimeUnit.MILLISECONDS.toSeconds(mVideoView.getDuration()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mVideoView.getDuration()))));
                seekBar.postDelayed(onEverySecond, 1000);

                videoCurrentTime.setText(String.format(" %02d:%02d ",
                        TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()),
                        TimeUnit.MILLISECONDS.toSeconds(mVideoView.getCurrentPosition()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()))));

            }
        });
    }
}
