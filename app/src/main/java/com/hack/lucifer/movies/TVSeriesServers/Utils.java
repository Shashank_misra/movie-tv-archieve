package com.hack.lucifer.movies.TVSeriesServers;

import android.util.Log;

import com.hack.lucifer.movies.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Utils {

    public static T_FetchContentVahid.Serial parseTVSerialBlob(JSONObject m) throws JSONException {

        T_FetchContentVahid.Serial serial = new T_FetchContentVahid.Serial();
        serial.name = m.getString("name");

        serial.iUrl = m.optString("iUrl");
        serial.rating = m.optString("rating");
        serial.description = m.optString("description");
        serial.runtime = m.optString("runtime");
        serial.genre = m.optString("genre");
        serial.actors = m.optString("actors");
        serial.released = m.optString("released");
        serial.writer = m.optString("writers");

        JSONArray seasonArray = m.getJSONArray("seasons");
        ArrayList<T_FetchContentVahid.Serial.Season> seasons = new ArrayList<>();

        for (int i = 0; i < seasonArray.length(); i++) {
            JSONObject season = seasonArray.getJSONObject(i);
            T_FetchContentVahid.Serial.Season seasonObj = new T_FetchContentVahid.Serial.Season();
            seasonObj.setName(season.getString("name"));
            seasonObj.setUrl(season.getString("url"));
            JSONArray episodeArray = season.getJSONArray("episodes");
            ArrayList<T_FetchContentVahid.Serial.Season.Episode> episodes = new ArrayList<>();

            for (int j = 0; j < episodeArray.length(); j++) {
                JSONObject episode = episodeArray.getJSONObject(j);
                T_FetchContentVahid.Serial.Season.Episode episodeObj = new T_FetchContentVahid.Serial.Season.Episode();
                episodeObj.setName(episode.getString("name"));
                episodeObj.setUrl(episode.getString("dUrl"));
                episodeObj.setID(episode.optString("EID"));
                Log.d("msg", "obtained json id: " + episodeObj.getID());
                episodes.add(episodeObj);
            }
            seasonObj.episodes.addAll(episodes);
            seasons.add(seasonObj);
        }
        serial.seasons.addAll(seasons);
        return serial;
    }

    public static MainActivity.OMDBResponse_TV parseOMDBDataForSerial(JSONObject json) {

        MainActivity.OMDBResponse_TV result;
        try {
            result = new MainActivity.OMDBResponse_TV();
            if (json.getString("Response").equals("True")) {
                result.title = json.getString("Title");
                result.description = json.getString("Plot");
                result.release = json.getString("Released");
                result.runtime = json.getString("Runtime");
                result.genre = json.getString("Genre");
                result.actors = json.getString("Actors");
                result.language = json.getString("Language");
                result.writer = json.getString("Writer");
//                Log.d("succ parse " + ++parseSucc + ": ", name);
            } else {
                result = null;
//                Log.d("parsing fail " + ++parseFailCount + ": ", name);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result = null;
        }

        return result;
    }

    public static JSONObject getTemplateJsonForTVSeries(ArrayList<T_FetchContentVahid.Serial> serials, String[] serverName, String[] serverURL, int position) {

        JSONObject server_1 = new JSONObject();
        JSONArray serialList = new JSONArray();
        try {
            server_1.put("serverName", serverName[0]);
            server_1.put("serverURL", serverURL[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < serials.size(); i++) {

            T_FetchContentVahid.Serial srl = serials.get(i);
            JSONObject a_serial = null;
            try {
                a_serial = new JSONObject();
                a_serial.put("name", srl.name);
                a_serial.put("rating", srl.rating);
                a_serial.put("iUrl", srl.iUrl);
                a_serial.put("description", srl.description);
                a_serial.put("genre", srl.genre);
                a_serial.put("actors", srl.actors);
                a_serial.put("released", srl.released);
                a_serial.put("language", srl.language);
                a_serial.put("runtime", srl.runtime);
                a_serial.put("writers", srl.writer);

                JSONArray seasons = new JSONArray();
                for (T_FetchContentVahid.Serial.Season season : srl.seasons) {

                    JSONObject sea = new JSONObject();
                    sea.put("name", season.getName());
                    sea.put("url", season.getUrl());
                    JSONArray episodes = new JSONArray();
                    for (T_FetchContentVahid.Serial.Season.Episode episode : season.episodes) {

                        JSONObject epi = new JSONObject();
                        epi.put("name", episode.name);
                        epi.put("dUrl", episode.url);
                        episodes.put(epi);
                    }
                    sea.put("episodes", episodes);
                    seasons.put(sea);
                }
                a_serial.put("seasons", seasons);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            serialList.put(a_serial);
        }
        try {
            server_1.put("list", serialList);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return server_1;
    }

}
