package com.hack.lucifer.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hack.lucifer.movies.MovieServers.M_CheckContentFarsi;
import com.hack.lucifer.movies.Movies.FinishedMovie;
import com.hack.lucifer.movies.TVSeriesServers.T_CheckContentTVSeries;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

public class SplashActivity extends AppCompatActivity {


    public static boolean goForSerialServers;
    public TextView updates;
    public ArrayList<FinishedMovie> movieDataFinal;
    public ArrayList<MainActivity.rawMovie> movieDataRaw = new ArrayList<>();
    public ArrayList<T_FetchContentVahid.Serial> serialData;
    private boolean startedWorking = true;
    private ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        MainActivity.movieFilePath = this.getFilesDir().getPath() + "/" + Constants.MOVIE_DATA_FILE_NAME;
        MainActivity.serialFilePath = this.getFilesDir().getPath() + "/" + Constants.TV_DATA_FILE_NAME;

        updates = (TextView) findViewById(R.id.updates);
        loading = (ProgressBar) findViewById(R.id.loadingContent);

        TextView title = (TextView) findViewById(R.id.title);
        if (title != null) {
            title.setTypeface(FontUtils.splashTitle);
        }
        ShouldAppWork check = new ShouldAppWork(this);
        check.execute();

        FrameLayout clickLayer = (FrameLayout) findViewById(R.id.clickableLayer);
        if (clickLayer != null) {
            clickLayer.setOnClickListener(new View.OnClickListener() {
                {

                }

                @Override
                public void onClick(View view) {
                    if (!startedWorking)
                        if (Utils.isConnected(SplashActivity.this)) {
                            if (loading != null) {
                                loading.setVisibility(View.VISIBLE);
                            }
                            ShouldAppWork shouldAppWork = new ShouldAppWork(SplashActivity.this);
                            shouldAppWork.execute();
                            startedWorking = true;
                        } else {
                            setUpdateMessage("No Network Connection. Tap to retry!");
                            if (loading != null) {
                                loading.setVisibility(View.GONE);
                            }
                        }
                }
            });
        }

    }

    public void setUpdateMessage(String msg) {

        if (updates != null)
            updates.setText(msg);
    }

    public boolean writeSerializedObject(Object object, String path) {

        try (
                OutputStream file = new FileOutputStream(path);
                OutputStream buffer = new BufferedOutputStream(file);
                ObjectOutput output = new ObjectOutputStream(buffer)
        ) {
            output.writeObject(object);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public Bundle readSerializedObject(String filePath) {

        Bundle response = new Bundle();
        Serializable data = null;
        try (
                InputStream dataFile = new FileInputStream(filePath);
                InputStream buffer = new BufferedInputStream(dataFile);
                ObjectInput input = new ObjectInputStream(buffer)
        ) {
            data = (Serializable) input.readObject();
            response.putString("msg", "Data Obtained");

        } catch (ClassNotFoundException | IOException ex) {
            ex.printStackTrace();
            response.putString("msg", "ERROR reading local data!\n" +
                    "Redirecting to servers...");
        }

        response.putSerializable("data", data);
        return response;
    }

    class AnalyticsData {

        int state;
        String dateCreated;
        ArrayList<String> logins;
        int failedLoginAttempts;
    }

    class DataManagerTask extends AsyncTask<Void, String, Integer> {

        private final int RESULT_FINE = 0;
        private final int RESULT_SINGLE_MATCH = 1;
        private final int RESULT_NO_MATCH = 2;

        SplashActivity callingClass;
        boolean serialFlag, movieFlag;

        DataManagerTask(Activity activity) {
            callingClass = (SplashActivity) activity;
            serialFlag = false;
            movieFlag = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            callingClass.setUpdateMessage("Looking Up local Cache...");
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            File file = new File(MainActivity.movieFilePath);
            if (file.exists()) {

                publishProgress("Found Movies! Reading...");
                Bundle response = callingClass.readSerializedObject(MainActivity.movieFilePath);
                callingClass.movieDataFinal = (ArrayList<FinishedMovie>) response.getSerializable("data");
                publishProgress("DONE!");
                if (callingClass.movieDataFinal == null) {
                    movieFlag = true;
                    publishProgress(response.getString("msg"));
                }
            } else {
                movieFlag = true;
                publishProgress("File Not Fount On System!");
            }

            file = new File(MainActivity.serialFilePath);
            if (file.exists()) {

                publishProgress("Found TV Serials! Reading...");
                Bundle response = callingClass.readSerializedObject(MainActivity.serialFilePath);
                callingClass.serialData = (ArrayList<T_FetchContentVahid.Serial>) response.getSerializable("data");
                publishProgress("DONE");
                if (callingClass.serialData == null) {
                    serialFlag = true;
                    publishProgress(response.getString("msg"));
                }
            } else {
                serialFlag = true;
                publishProgress("File Not Fount On System!");
            }


            if (movieFlag && serialFlag)
                return RESULT_NO_MATCH;
            else if (movieFlag || serialFlag)
                return RESULT_SINGLE_MATCH;
            else
                return RESULT_FINE;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            callingClass.setUpdateMessage(values[0]);
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            switch (result) {
                case RESULT_FINE:

                    Intent intent = new Intent(callingClass, MainActivity.class);
                    MainActivity.globalReadyMovies = callingClass.movieDataFinal;
                    MainActivity.globalReadySerials = callingClass.serialData;
                    MainActivity.databaseIsUpdated = true;
//                    intent.putExtra("movieData",callingClass.movieDataFinal);
//                    intent.putExtra("serialData",callingClass.serialData);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("movieData", callingClass.movieDataFinal);
//                    bundle.putSerializable("serialData", callingClass.serialData);
//                    intent.putExtras(bundle);
//                    Log.d("msg", String.valueOf(callingClass.serialData.get(0) != null));
                    startActivity(intent);
                    callingClass.finish();

                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {

                            for (T_FetchContentVahid.Serial serial : MainActivity.globalReadySerials)
                                MainActivity.serialMap.put(serial.name, serial);

                            for (FinishedMovie movie : MainActivity.globalReadyMovies)
                                MainActivity.movieMap.put(movie.title, movie);

                            MainActivity.mapsAreReady = true;
                        }
                    });
                    thread.start();
                    break;

                case RESULT_NO_MATCH:

                    if (Utils.isConnected(callingClass)) {
                        M_CheckContentFarsi check1 = new M_CheckContentFarsi(callingClass);
                        goForSerialServers = true;
                        check1.execute();
                    } else {
                        setUpdateMessage("No Network Connection. Tap to retry!");
                        startedWorking = false;
                        if (callingClass.loading != null) {
                            callingClass.loading.setVisibility(View.GONE);
                        }
                    }
                    break;

                case RESULT_SINGLE_MATCH:

                    if (Utils.isConnected(callingClass)) {
                        if (movieFlag) {
                            M_CheckContentFarsi check = new M_CheckContentFarsi(callingClass);
                            goForSerialServers = false;
                            check.execute();
                        } else {
                            T_CheckContentTVSeries checkTV = new T_CheckContentTVSeries(callingClass);
                            checkTV.execute();
                        }
                    } else {
                        setUpdateMessage("No Network Connection. Tap to retry!");
                        startedWorking = false;
                        if (callingClass.loading != null) {
                            callingClass.loading.setVisibility(View.GONE);
                        }
                    }
                    break;
            }

        }
    }

    class ShouldAppWork extends AsyncTask<Void, String, Boolean> {

        SplashActivity callingClass;
        boolean flagFatal = false;

        public ShouldAppWork(SplashActivity splashActivity) {
            this.callingClass = splashActivity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            publishProgress("Verifying App Integrity!");
            flagFatal = false;
            int resCode;
            InputStream in;
            String result;
            try {
                URL url = new URL(Constants.BLOB_ANALYTICS);
                URLConnection urlConn = url.openConnection();

                HttpsURLConnection httpsConn = (HttpsURLConnection) urlConn;
                httpsConn.setAllowUserInteraction(false);
                httpsConn.setInstanceFollowRedirects(true);
                httpsConn.setRequestMethod("GET");
                httpsConn.connect();
                resCode = httpsConn.getResponseCode();
                if (resCode == HttpURLConnection.HTTP_OK) {
                    in = httpsConn.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            in, "UTF-8"));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    in.close();
                    result = sb.toString();
                    Log.d("msg", result);

                    JSONObject wrapper = new JSONObject(result);
                    int state = wrapper.getInt("globalState");
                    JSONArray array = wrapper.getJSONArray("data");

                    HashMap<String, AnalyticsData> entries = new HashMap<>();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject obj = array.getJSONObject(i);
                            AnalyticsData data = new AnalyticsData();
                            data.state = obj.getInt("state");
                            data.dateCreated = obj.getString("created");
                            data.failedLoginAttempts = obj.getInt("failCount");
                            data.logins = new ArrayList<>();
                            JSONArray loginsJsonArray = obj.getJSONArray("logins");
                            for (int k = 0; k < loginsJsonArray.length(); k++)
                                data.logins.add(loginsJsonArray.getString(k));

                            entries.put(obj.getString("id"), data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (entries.get(AppController.DEVICE_ID) != null) {
                        if (entries.get(AppController.DEVICE_ID).state == 0) {

                            publishProgress("Servers are on a break!");
                            flagFatal = true;
                            entries.get(AppController.DEVICE_ID).failedLoginAttempts++;
                        }
                        String dateToday = getTodaysDate();
                        entries.get(AppController.DEVICE_ID).logins.add(dateToday);
                    } else {
                        AnalyticsData data = new AnalyticsData();
                        data.failedLoginAttempts = 0;
                        data.logins = new ArrayList<>();
                        data.logins.add(getTodaysDate());
                        data.state = state;
                        data.dateCreated = getTodaysDate();
                        entries.put(AppController.DEVICE_ID, data);
                    }

                    JSONArray upload = new JSONArray();
                    Set<String> keys = entries.keySet();
                    for (String key : keys) {

                        JSONObject obj = new JSONObject();
                        obj.put("id", key);
                        obj.put("state", entries.get(key).state);
                        obj.put("failCount", entries.get(key).failedLoginAttempts);
                        obj.put("created", entries.get(key).dateCreated);

                        JSONArray loginJsonArray = new JSONArray();
                        for (String date : entries.get(key).logins)
                            loginJsonArray.put(date);
                        obj.put("logins", loginJsonArray);

                        upload.put(obj);
                    }

                    JSONObject uploadWrapper = new JSONObject();
                    uploadWrapper.put("globalState", state);
                    uploadWrapper.put("data", upload);
                    Log.d("msg", "uploading: " + uploadWrapper);

                    JsonObjectRequest putOnServer = new JsonObjectRequest(Request.Method.PUT,
                            Constants.BLOB_ANALYTICS,
                            uploadWrapper,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    if (!flagFatal) {
                                        publishProgress("VALID");
                                        DataManagerTask start = new DataManagerTask(callingClass);
                                        start.execute();
                                    } else {
                                        callingClass.setUpdateMessage("Please Try After Sometime");
                                        startedWorking = true;
                                        Thread close = new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    Thread.sleep(1000);
                                                } catch (InterruptedException e) {
                                                    e.printStackTrace();
                                                }
                                                callingClass.finish();
                                            }
                                        });
                                        close.start();
                                    }
                                    callingClass.loading.setVisibility(View.GONE);
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            publishProgress("_");
                        }
                    });
                    AppController.getInstance().addToRequestQueue(putOnServer);
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                publishProgress("_");
            }
            return null;
        }

        private String getTodaysDate() {

            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //Or whatever format fits best your needs.
            return sdf.format(date);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            if (values[0].equals("_")) {
                callingClass.setUpdateMessage("Connection Error. Tap to Retry!");
            } else
                callingClass.setUpdateMessage(values[0]);

            startedWorking = false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }
}
