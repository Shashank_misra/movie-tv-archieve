package com.hack.lucifer.movies.Serials;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hack.lucifer.movies.Databases.Download_DB_helper;
import com.hack.lucifer.movies.Databases.Serial_DB_Helper;


public class DBManager {

    public static int FATAL_ERROR = 404;
    Context activityContext;
    Serial_DB_Helper worker;

    public DBManager(Context context) {
        activityContext = context;
        worker = new Serial_DB_Helper(activityContext);
    }

    public int addEpisode(String SerialName, int seasonNum, String videoId, int epiNum, String url) {

        SQLiteDatabase db = worker.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Serial_DB_Helper.SERIAL, SerialName);
        contentValues.put(Serial_DB_Helper.VIDEO_ID, videoId);
        contentValues.put(Serial_DB_Helper.SEASON_NUM, seasonNum);
        contentValues.put(Serial_DB_Helper.PLAY_TIME, 0);
        contentValues.put(Serial_DB_Helper.FLAG_OFFLINE, 0);
        contentValues.put(Serial_DB_Helper.FLAG_WATCHED, 0);
        contentValues.put(Serial_DB_Helper.EPISODE_NUM, epiNum);
        contentValues.put(Serial_DB_Helper.URL, url);
        contentValues.putNull(Serial_DB_Helper.STORE_PATH);

        long res = db.insert(Serial_DB_Helper.TABLE_SERIALS, null, contentValues);
        db.close();
        return (int) res;

    }

    public boolean isExisting(String videoId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor res = db.rawQuery("Select COUNT(*) from " + Serial_DB_Helper.TABLE_SERIALS + " where " + Serial_DB_Helper.VIDEO_ID + " = ?", new String[]{videoId});
        res.moveToFirst();
        db.close();
        return res.getInt(0) == 1;
    }

    public boolean updateOfflineLocation(String videoId, String path) {

        if (isExisting(videoId)) {

            SQLiteDatabase db = worker.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            if (path == null) {
                contentValues.putNull(Serial_DB_Helper.STORE_PATH);
                contentValues.put(Serial_DB_Helper.FLAG_OFFLINE, 0);
            } else {
                contentValues.put(Serial_DB_Helper.STORE_PATH, path);
                contentValues.put(Serial_DB_Helper.FLAG_OFFLINE, 1);
            }
            int res = db.update(Serial_DB_Helper.TABLE_SERIALS, contentValues, Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoId});
            db.close();
            return res == 1;
        } else return false;
    }

    public int updatePlayedTillTime(String videoId, long time) {

        if (isExisting(videoId)) {

            SQLiteDatabase db = worker.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(Serial_DB_Helper.PLAY_TIME, time);
            return db.update(Serial_DB_Helper.TABLE_SERIALS, contentValues, Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoId});
        } else return -1;
    }

    public long getPlayedDuration(String videoId) {

        if (isExisting(videoId)) {

            SQLiteDatabase db = worker.getReadableDatabase();
            Cursor res = db.query(true, Serial_DB_Helper.TABLE_SERIALS, new String[]{Serial_DB_Helper.PLAY_TIME},
                    Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoId}, null, null, null, null);
            res.moveToFirst();
            return (long) res.getInt(res.getColumnIndex(Serial_DB_Helper.PLAY_TIME));
        } else return -1;
    }

    public int markAsWatched(String videoId) {

        if (isExisting(videoId)) {

            SQLiteDatabase db = worker.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(Serial_DB_Helper.FLAG_WATCHED, 1);
            return db.update(Serial_DB_Helper.TABLE_SERIALS, contentValues, Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoId});
        } else return -1;
    }

    public int getLastPlayedEpisode(String serialName, int seasonNum) {


        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor res = db.query(Serial_DB_Helper.TABLE_SERIALS, new String[]{"max(" + Serial_DB_Helper.EPISODE_NUM + ") as MAX_EPI"},
                Serial_DB_Helper.SERIAL + " = ? AND " + Serial_DB_Helper.SEASON_NUM + " = ? AND " + Serial_DB_Helper.FLAG_WATCHED + " = ?",
                new String[]{serialName, String.valueOf(seasonNum), String.valueOf(1)}, null, null, null);
        res.moveToFirst();
        return res.getInt(res.getColumnIndex("MAX_EPI"));
    }

    public boolean availableOffline(String videoId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor res = db.query(true, Serial_DB_Helper.TABLE_SERIALS, new String[]{Serial_DB_Helper.FLAG_OFFLINE},
                Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoId}, null, null, null, null);
        res.moveToFirst();
        return res.getInt(res.getColumnIndex(Serial_DB_Helper.FLAG_OFFLINE)) == 1;
    }

    public String getOfflineLocation(String videoId) {

        if (isExisting(videoId)) {
            if (availableOffline(videoId)) {

                SQLiteDatabase db = worker.getReadableDatabase();
                Cursor res = db.query(true, Serial_DB_Helper.TABLE_SERIALS, new String[]{Serial_DB_Helper.STORE_PATH},
                        Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoId}, null, null, null, null);
                res.moveToFirst();
                try {
                    return res.getString(res.getColumnIndex(Serial_DB_Helper.STORE_PATH));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return "";
                }
            } else return "";
        } else return "";
    }


    public String getVideoURL(String videoId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor res = db.query(true, Serial_DB_Helper.TABLE_SERIALS, new String[]{Serial_DB_Helper.URL},
                Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoId}, null, null, null, null);
        res.moveToFirst();
        try {
            return res.getString(res.getColumnIndex(Serial_DB_Helper.URL));
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public Download_DB_helper.STATES getStateForVideID(String id) {

        return null;
    }

    public int putNullToStorePath(String videoID) {

        if (isExisting(videoID)) {

            SQLiteDatabase db = worker.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.putNull(Serial_DB_Helper.STORE_PATH);
            int val = db.update(Serial_DB_Helper.TABLE_SERIALS, contentValues, Serial_DB_Helper.VIDEO_ID + " = ? ", new String[]{videoID});
            db.close();
            db.releaseReference();
            return val;
        } else return -1;
    }

    public String[][] getStoragePathsWithVideoID() {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Serial_DB_Helper.STORE_PATH +
                ", " + Serial_DB_Helper.VIDEO_ID +
                " from " + Serial_DB_Helper.TABLE_SERIALS + " where " + Serial_DB_Helper.STORE_PATH
                + " is NOT NULL", new String[]{});
        String[][] result = new String[cursor.getCount()][2];
        cursor.moveToFirst();
        int i = 0;
        if (result.length > 0)
            do {
                result[i][0] = cursor.getString(cursor.getColumnIndex(Serial_DB_Helper.STORE_PATH));
                result[i++][1] = cursor.getString(cursor.getColumnIndex(Serial_DB_Helper.VIDEO_ID));
            } while (cursor.moveToNext());
        else
            result = null;
        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }
}
