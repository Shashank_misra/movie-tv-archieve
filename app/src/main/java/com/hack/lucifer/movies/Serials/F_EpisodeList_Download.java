package com.hack.lucifer.movies.Serials;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.Databases.Download_DB_helper;
import com.hack.lucifer.movies.R;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;

import java.io.File;
import java.util.ArrayList;


public class F_EpisodeList_Download extends Fragment {

    T_FetchContentVahid.Serial.Season displaySeason;
    T_FetchContentVahid.Serial aboutSerial;
    DownloadManager downloadManager;
    com.hack.lucifer.movies.DownloadManager.DBManager downDBManager;
    DBManager serialDBManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        A_KeepOffline activity = (A_KeepOffline) getActivity();
        downDBManager = new com.hack.lucifer.movies.DownloadManager.DBManager(getActivity());
        downloadManager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        serialDBManager = new DBManager(activity);
        aboutSerial = activity.obtainedSerial;


        View rootView = inflater.inflate(R.layout.f_download_episodelist, container, false);
        RecyclerView episodeList = (RecyclerView) rootView.findViewById(R.id.episodeList);
        ArrayList<rowStructure> listData = obtainData(displaySeason);

        episodeList.setAdapter(new mAdapter(listData, activity));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        episodeList.setLayoutManager(mLayoutManager);
        episodeList.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    private ArrayList<rowStructure> obtainData(T_FetchContentVahid.Serial.Season displaySeason) {

        if (displaySeason == null)
            getActivity().finish();

        ArrayList<rowStructure> result = new ArrayList<>();
        for (T_FetchContentVahid.Serial.Season.Episode episode : displaySeason.episodes) {

            Download_DB_helper.STATES state = downDBManager.getVideoStateForVideoID(episode.getID());
            rowStructure row = new rowStructure(episode.name, episode.url, episode.getID(), state);
            result.add(row);
        }
        return result;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        displaySeason = (T_FetchContentVahid.Serial.Season) args.getSerializable("data");
    }

    class rowStructure {
        String name, url, videoId;
        Download_DB_helper.STATES state;

        rowStructure(String name, String url, String videoId, Download_DB_helper.STATES state) {
            this.name = name;
            this.url = url;
            this.state = state;
            this.videoId = videoId;
        }
    }

    public class mAdapter extends RecyclerView.Adapter<mAdapter.Holder> {

        private final ArrayList<rowStructure> data;
        private A_KeepOffline activity;

        mAdapter(ArrayList<rowStructure> data, A_KeepOffline activity) {
            this.data = data;
            this.activity = activity;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.row_episodelist_downloads, parent, false);
            return new Holder(itemView);
        }

        @Override
        public void onBindViewHolder(final Holder holder, int position) {

            Log.d("msg", "epi name: " + data.get(position).name);
            holder.name.setText(data.get(position).name);

            Download_DB_helper.STATES state = downDBManager.getVideoStateForVideoID(data.get(position).videoId);

            if (state.getVal() == Download_DB_helper.STATES.ONGOING.getVal() ||
                    state.getVal() == Download_DB_helper.STATES.PENDING.getVal() ||
                    state.getVal() == Download_DB_helper.STATES.PAUSED.getVal()) {

                holder.remove.setVisibility(View.GONE);
                holder.downloaded.setVisibility(View.GONE);
                holder.download.setVisibility(View.VISIBLE);
                holder.enqueued = true;

            } else if (state.getVal() == Download_DB_helper.STATES.FAILED.getVal() ||
                    state.getVal() == Download_DB_helper.STATES.UNAVAILABLE.getVal()) {

                holder.download.setVisibility(View.VISIBLE);
                holder.remove.setVisibility(View.GONE);
                holder.downloaded.setVisibility(View.GONE);
                holder.enqueued = false;

            } else if (state.getVal() == Download_DB_helper.STATES.COMPLETE.getVal()) {

                holder.download.setVisibility(View.GONE);
                holder.remove.setVisibility(View.VISIBLE);
                holder.downloaded.setVisibility(View.VISIBLE);
                holder.enqueued = false;

            }

            if (holder.enqueued) {
                holder.download.setImageResource(R.drawable.ic_queue_black_24dp);
                holder.download.setEnabled(false);
            } else {
                holder.download.setImageResource(R.drawable.ic_download_black_50dp);
                holder.download.setEnabled(true);
            }

            holder.download.setOnClickListener(new View.OnClickListener() {
                {

                }

                @Override
                public void onClick(View view) {

                    Object result[] = addVideoToDownloadManager(data.get(holder.getAdapterPosition()));
                    final long downloadId = (long) result[0];
                    final String filePath = (String) result[1];

                    if (downloadId != -1) {

                        AppController.feedback();
                        holder.download.setVisibility(View.VISIBLE);
                        holder.download.setImageResource(R.drawable.ic_queue_black_24dp);
                        holder.download.setEnabled(false);
                        holder.enqueued = true;

                        Log.d("msg", "added download id: " + downloadId + " for videoid: " + data.get(holder.getAdapterPosition()).videoId);
                    /*
                    Determining state of download just added: ONGOING or PENDING
                     */
                        DownloadManager.Query q = new DownloadManager.Query();
                        q.setFilterById(downloadId);
                        Cursor cursor = downloadManager.query(q);
                        cursor.moveToFirst();
                        int state_db = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                        int sizeInBytes = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                        String unit_t = "B";
                        if (sizeInBytes > 1024 + 100) {

                            sizeInBytes = sizeInBytes / 1024;
                            unit_t = "KB";
                            if (sizeInBytes > 1024 + 100) {

                                sizeInBytes = sizeInBytes / 1024;
                                unit_t = "MB";
                                if (sizeInBytes > 1024 + 100) {

                                    sizeInBytes = sizeInBytes / 1024;
                                    unit_t = "GB";
                                }
                            }
                        }
                        String size = sizeInBytes + unit_t;
                        Download_DB_helper.STATES state = getNativeStateCorrespondingAndroidDownloadManager(state_db);
                        Log.d("msg", "state added: " + state);
                    /*
                    Update downloads database
                     */
                        downDBManager.addDownload(
                                downloadId,
                                data.get(holder.getAdapterPosition()).videoId,
                                state,
                                data.get(holder.getAdapterPosition()).name,
                                size,
                                data.get(holder.getAdapterPosition()).url,
                                filePath);
                    /*
                    Save the Id to shared preferences along with state
                     */
                        final SharedPreferences setting = activity.getSharedPreferences(Constants.DOWNLOAD_ID_PREFERENCE_FILE,
                                Context.MODE_APPEND);
                        final SharedPreferences.Editor edit = setting.edit();
                        edit.putLong(downloadId + "", state_db);
                        edit.commit();
                    }
                }
            });

        }

        private Download_DB_helper.STATES getNativeStateCorrespondingAndroidDownloadManager(int state_db) {

            switch (state_db) {
                case DownloadManager.STATUS_FAILED:
                    return Download_DB_helper.STATES.FAILED;
                case DownloadManager.STATUS_PAUSED:
                    return Download_DB_helper.STATES.PAUSED;
                case DownloadManager.STATUS_PENDING:
                    return Download_DB_helper.STATES.PENDING;
                case DownloadManager.STATUS_RUNNING:
                    return Download_DB_helper.STATES.ONGOING;
                case DownloadManager.STATUS_SUCCESSFUL:
                    return Download_DB_helper.STATES.COMPLETE;
            }
            return null;
        }

        private Object[] addVideoToDownloadManager(rowStructure row) {

            final String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("We need permissions to save video in your device!");
                    builder.setTitle("Permission to save Videos");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(), permissions, 0);
                        }
                    });

                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(getActivity(), permissions, 0);
                }
            }

            Object[] result;
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Uri Download_Uri = Uri.parse(row.url);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE);
                request.setTitle(row.name);
                request.setDescription("Movie TV");
                request.allowScanningByMediaScanner();

//            request.setDestinationInExternalFilesDir(activity, Environment.DIRECTORY_DOWNLOADS, row.name);
//
//            SharedPreferences preferences = getActivity().getSharedPreferences(AppController.packageName,
//                    Context.MODE_PRIVATE);
//            String location = preferences.getString(Constants.SP_STORAGE_TAG, "");
                String filePath = File.separator +
                        Constants.DIRECTORY_NAME + File.separator +
                        aboutSerial.name + File.separator +
                        displaySeason.getName() + File.separator +
                        row.name;


                Log.d("msg", Environment.getExternalStorageDirectory() + "\n" +
                        Environment.getExternalStorageDirectory().getAbsolutePath());
                request.setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_DOWNLOADS, filePath);

                result = new Object[]{downloadManager.enqueue(request), filePath};
            } else
                result = new Object[]{-1L, ""};

            return result;
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class Holder extends RecyclerView.ViewHolder {

            public TextView name;
            public ImageView remove, downloaded, download;
            public boolean enqueued = false;

            public Holder(View itemView) {

                super(itemView);

                name = (TextView) itemView.findViewById(R.id.name);
                remove = (ImageView) itemView.findViewById(R.id.remove);
                downloaded = (ImageView) itemView.findViewById(R.id.downloadComplete);
                download = (ImageView) itemView.findViewById(R.id.downloadNow);
            }
        }

    }

}
