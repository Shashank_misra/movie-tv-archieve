package com.hack.lucifer.movies.TVSeriesServers;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.MainActivity;
import com.hack.lucifer.movies.SplashActivity;
import com.hack.lucifer.movies.Utils;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class T_FetchContentVahid extends AsyncTask<Void, String, String> {

    SplashActivity callingClass;
    String serverUrl;

    public T_FetchContentVahid(Activity activity, String serverUrl) {
        this.callingClass = (SplashActivity) activity;
        this.serverUrl = serverUrl;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callingClass.setUpdateMessage("Connecting to TV Server 1");
    }

    @Override
    protected String doInBackground(Void... voids) {

        /*
        Fetch series list
         */
        Document document = null;
        ArrayList<Serial> serials = new ArrayList<>();
        try {
            document = Jsoup.connect(serverUrl).timeout(60000).get();
            Elements links = document.select("a[href]");

            int g = 0;
            for (; g < links.size(); g++)
                if (links.get(g).text().equalsIgnoreCase("../"))
                    break;

            for (; g < links.size(); g++) {

                Element link = links.get(g);
                String name = link.text();
                String url = link.attr("abs:href");

                if (!name.equalsIgnoreCase("../")) {
                    name = name.replace("/", "");
                    name = name.trim();
                    serials.add(new Serial(name, url));
                }
                publishProgress("Found " + serials.size() + " links");
            }

        } catch (IOException e) {
            e.printStackTrace();
            publishProgress("Crashed while Connecting to base");
            return null;
        }

        /*
        Fetch seasons for each series
         */
        for (Serial serial : serials) {
            try {
                document = Jsoup.connect(serial.url).timeout(60000).get();
                publishProgress("Finding Seasons for\n" + serial.name);
                Elements links = document.select("a[href]");

                int g = 0;
                for (; g < links.size(); g++)
                    if (links.get(g).text().equalsIgnoreCase("../"))
                        break;

                for (; g < links.size(); g++) {

                    Element link = links.get(g);
                    String name = link.text();
                    String url = link.attr("abs:href");
                    if (!name.equalsIgnoreCase("../") && !name.equalsIgnoreCase("sub")) {

                        name = name.replace("/", "");
                        name = name.trim();
                        serial.seasons.add(new Serial.Season(name, url));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /*
        Fetch the episodes for all seasons of all series << HEAVIEST TASK !!!>>
         */
        for (Serial serial : serials) {
            for (Serial.Season season : serial.seasons) {

                try {
                    Log.d("msg", season.url);
                    document = Jsoup.connect(season.url).timeout(60000).get();
                    Elements links = document.select("a[href]");

                    String proceedToUrl = "";
                    boolean shouldProceed = false;
                    for (Element link : links) {

                        String name = link.text();
                        String url = link.attr("abs:href");
                        if (name.equalsIgnoreCase("480p/")) {
                            proceedToUrl = url;
                            shouldProceed = true;
                        } else if (name.equalsIgnoreCase("720p/") && !shouldProceed) {
                            proceedToUrl = url;
                            shouldProceed = true;
                        } else if (name.equalsIgnoreCase("1080p/") && !shouldProceed) {
                            proceedToUrl = url;
                            shouldProceed = true;
                        }
                    }
                    if (shouldProceed) {
                        document = Jsoup.connect(proceedToUrl).timeout(60000).get();
                        links = document.select("a[href]");
                    }

                    int g = 0;
                    for (; g < links.size(); g++)
                        if (links.get(g).text().equalsIgnoreCase("../"))
                            break;

                    for (; g < links.size(); g++) {

                        Element link = links.get(g);
                        String name = link.text();
                        String url = link.attr("abs:href");
                        if (!name.equalsIgnoreCase("../") && !name.endsWith(".zip") && !name.equalsIgnoreCase("sub")) {
                            name = name.trim();
                            season.episodes.add(new Serial.Season.Episode(name, url));
                            publishProgress("" + season.name + ": " + name);
//                            Log.d("msg", res + " name: " + name + " url: " + url);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        /*
        Fetching Metadata for Serials
         */
        long count[] = new long[1];
        for (Serial s : callingClass.serialData) {
            try {
                String query_url = "http://www.omdbapi.com/?" +
                        "t=" + s.name;
                query_url += "&y=&plot=short&r=json";

                JSONObject response = Utils.getJSONObjectFromUrl(query_url);
//                Log.d("msg", "returned with: " + response);
                if (response != null) {
                    MainActivity.OMDBResponse_TV result = com.hack.lucifer.movies.TVSeriesServers.Utils.parseOMDBDataForSerial(response);

                    if (result != null) {

                        s.name = result.title;
                        s.description = result.description;
                        s.released = result.release;
                        s.actors = result.actors;
                        s.genre = result.genre;
                        s.writer = result.writer;
                        s.runtime = result.runtime;
                        count[0]++;
                    }
                    publishProgress("O: Metadata found: " + count[0]);
                }
            } catch (Exception e) {
                publishProgress("crashed for " + s.name);
                e.printStackTrace();
            }
        }

        publishProgress("Caching data to local Server");
//            Collections.sort(globalReadyMovies);
        JsonObjectRequest putOnServer = new JsonObjectRequest(Request.Method.PUT,
                Constants.BLOBID_TVSERIES_VAHID,
                com.hack.lucifer.movies.TVSeriesServers.Utils.getTemplateJsonForTVSeries(serials, new String[]{"Vahid"}, new String[]{serverUrl}, 0),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callingClass.setUpdateMessage("Updated data to server!");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callingClass.setUpdateMessage("ERROR updating data to server");
            }
        });
        AppController.getInstance().addToRequestQueue(putOnServer);

        for (Serial s : serials)
            Log.d("msg1", s.toString());
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        if (values[0].equals("_")) {
            callingClass.setUpdateMessage("EXCEPTION at FE_CO_VA");
            Toast.makeText(callingClass, "CRASHED", Toast.LENGTH_SHORT).show();
        } else {
            callingClass.setUpdateMessage(values[0]);
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        boolean response = callingClass.writeSerializedObject(callingClass.movieDataFinal, MainActivity.serialFilePath);
        Log.d("msg", "writing serial serialized Object response >Serial>Fetch: " + response);

        Intent intent = new Intent(callingClass, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("movieDataFinal", callingClass.movieDataFinal);
        bundle.putSerializable("serialData", callingClass.serialData);
        intent.putExtras(bundle);
        callingClass.startActivity(intent);
        callingClass.finish();
    }

    public static class Serial implements Comparable, Serializable {

        public String name, url, iUrl, description, rating, genre, actors, released, language, runtime, writer;
        public ArrayList<Season> seasons = new ArrayList<>();

        public Serial(String name, String url) {
            this.name = name;
            this.url = url;
        }

        public Serial() {

        }

        @Override
        public int compareTo(@NonNull Object o) {

            Serial s = (Serial) o;
            return this.name.compareTo(s.name);
        }

        @Override
        public String toString() {

            String s = "N: " + name + "   U: " + url;

            for (Season season : this.seasons)
                s = s + "\n" + season.name + " " + season.episodes.size();

            for (Season season : this.seasons) {

                s = s + "\n\t" + season.name + "   epiCount: " + season.episodes.size() + "   U:" + season.url;
                for (Season.Episode episodes : season.episodes) {
                    s = s + "\n\t\tN: " + episodes.name + "   U:" + episodes.url;
                }
            }
            return s;
        }

        public static class Season implements Serializable {

            public int seasonNum;
            public ArrayList<Episode> episodes = new ArrayList<>();
            private String name, url;

            public Season(String name, String url) {
                this.name = name;
                this.url = url;

                setSeasonNum(0);
            }

            public Season() {

            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
                setSeasonNum(0);
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public int getSeasonNum() {
                return seasonNum;
            }

            private void setSeasonNum(int seasonNum_) {

                try {
                    seasonNum = Integer.parseInt((name.split(" "))[1]);
                } catch (Exception ignored) {
                    seasonNum = -1;
                }
            }

            public static class Episode implements Serializable {

                public int episodeNumber = -1;
                public String name, url, print, format, displayName;
                private String ID = null;

                public String getID() {
                    return ID;
                }

                public void setID(String ID) {
                    this.ID = ID;
                }

                public Episode(String name, String url) {
                    this.name = name;
                    this.url = url;

                    setPrint("");
                    setFormat("");
                    setEpisodeNumber(0);
                    setDisplayName("");
                }

                public Episode() {

                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getPrint() {
                    return print;
                }

                private void setPrint(String print_) {

                    if (this.name.contains("480"))
                        print = "480p";
                    else if (this.name.contains("720"))
                        print = "720p";
                    else if (this.name.contains("1080"))
                        print = "1080p";
                }

                public String getFormat() {
                    return format;
                }

                private void setFormat(String format_) {
                    format = name.split("[.]")[name.split("[.]").length - 1];
                }

                public String getDisplayName() {
                    return displayName;
                }

                private void setDisplayName(String displayName_) {
                    displayName = "Episode " + (0 < episodeNumber && episodeNumber < 10 ?
                            "0" + episodeNumber : episodeNumber);
                }

                public int getEpisodeNumber() {
                    return episodeNumber;
                }

                private void setEpisodeNumber(int episodeNumber_) {
                    this.name = name.toUpperCase();
                    for (int i = 1; i < 50; i++) {
                        String hint = "E" + (i < 10 ? "0" + i : i);

                        if (this.name.contains(hint)) {
                            episodeNumber = i;
                            break;
                        }
                    }
                }

                public void setName(String name) {
                    this.name = name;
                    setPrint("");
                    setFormat("");
                    setEpisodeNumber(0);
                    setDisplayName("");

                }
            }
        }
    }
}
