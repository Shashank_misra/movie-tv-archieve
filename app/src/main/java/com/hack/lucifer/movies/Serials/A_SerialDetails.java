package com.hack.lucifer.movies.Serials;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.R;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;
import com.hack.lucifer.movies.Utils;

public class A_SerialDetails extends AppCompatActivity {

    public static T_FetchContentVahid.Serial obtainedSerial;
    LayoutInflater inflater;
    RecyclerView seasonList;
    public static boolean bitmapAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial_details);

        Intent received = getIntent();
        obtainedSerial = (T_FetchContentVahid.Serial) received.getSerializableExtra("serial");

        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        initialize();
        addInformation();
        addWatchNow();
    }

    private void initialize() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        ImageView posterBg = (ImageView) findViewById(R.id.posterBg);
        ImageView poster = (ImageView) findViewById(R.id.poster);

        if (bitmapAvailable) {
            if (posterBg != null) {
                posterBg.setImageBitmap(AppController.b1);
            }
            if (poster != null) {
                poster.setImageBitmap(AppController.b2);
            }
        } else {
            Utils.imageLoader.displayImage(obtainedSerial.iUrl, posterBg, Utils.options);
            Utils.imageLoader.displayImage(obtainedSerial.iUrl, poster, Utils.options);
        }

        TextView countSeasons = (TextView) findViewById(R.id.countSeasons);
        if (countSeasons != null) {
            countSeasons.setText(obtainedSerial.seasons.size() + "");
        }

        // for setting title when toolbar is collapsed
//        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
//        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
//        if (appBarLayout != null) {
//            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//                boolean isShow = false;
//                int scrollRange = -1;
//
//                @Override
//                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                    if (scrollRange == -1) {
//                        scrollRange = appBarLayout.getTotalScrollRange();
//                    }
//                    if (scrollRange + verticalOffset == 0) {
//                        if (collapsingToolbarLayout != null) {
//                            collapsingToolbarLayout.setTitle(obtainedSerial.name);
//                        }
//                        isShow = true;
//                    } else if (isShow) {
//                        if (collapsingToolbarLayout != null) {
//                            collapsingToolbarLayout.setTitle(" ");
//                        }
//                        isShow = false;
//                    }
//                }
//            });
//        }

        getSupportActionBar().setTitle(obtainedSerial.name);
        Button keepOffline = (Button) findViewById(R.id.download);
        if (keepOffline != null) {
            keepOffline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(A_SerialDetails.this, A_KeepOffline.class);
                    intent.putExtra("data", obtainedSerial);
                    startActivity(intent);
                }
            });
        }
    }

    private void addInformation() {

        TableLayout metadataTable = (TableLayout) findViewById(R.id.metadataTable);
        if (obtainedSerial.released != null) {
            View row1 = inflater.inflate(R.layout.table_row_metadata_serial, metadataTable, false);
            TextView header = (TextView) row1.findViewById(R.id.contentC1);
            TextView content = (TextView) row1.findViewById(R.id.contentC2);
            header.setText("Released");
            content.setText(obtainedSerial.released);
            metadataTable.addView(row1);
        } else
            Log.d("msg", "released is null");

        if (obtainedSerial.description != null) {
            View row1 = inflater.inflate(R.layout.table_row_metadata_serial, metadataTable, false);
            TextView header = (TextView) row1.findViewById(R.id.contentC1);
            TextView content = (TextView) row1.findViewById(R.id.contentC2);
            header.setText("Plot");
            content.setText(obtainedSerial.description);
            metadataTable.addView(row1);
        } else
            Log.d("msg", "description is null");

        if (obtainedSerial.actors != null) {
            View row1 = inflater.inflate(R.layout.table_row_metadata_serial, metadataTable, false);
            TextView header = (TextView) row1.findViewById(R.id.contentC1);
            TextView content = (TextView) row1.findViewById(R.id.contentC2);
            header.setText("Actors");
            content.setText(obtainedSerial.actors);
            metadataTable.addView(row1);
        } else
            Log.d("msg", "actors is null");

        if (obtainedSerial.runtime != null) {
            View row1 = inflater.inflate(R.layout.table_row_metadata_serial, metadataTable, false);
            TextView header = (TextView) row1.findViewById(R.id.contentC1);
            TextView content = (TextView) row1.findViewById(R.id.contentC2);
            header.setText("Duration");
            content.setText(obtainedSerial.runtime);
            metadataTable.addView(row1);
        } else
            Log.d("msg", "runtime is null");

        if (obtainedSerial.genre != null) {
            View row1 = inflater.inflate(R.layout.table_row_metadata_serial, metadataTable, false);
            TextView header = (TextView) row1.findViewById(R.id.contentC1);
            TextView content = (TextView) row1.findViewById(R.id.contentC2);
            header.setText("Genre");
            content.setText(obtainedSerial.genre);
            metadataTable.addView(row1);
        } else
            Log.d("msg", "genre is null");
    }

    private void addWatchNow() {

        seasonList = (RecyclerView) findViewById(R.id.seasonList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        seasonList.setLayoutManager(layoutManager);
        seasonList.setItemAnimator(new DefaultItemAnimator());
        seasonList.setAdapter(new RecyclerView.Adapter() {

            class viewHolder extends RecyclerView.ViewHolder {
                public TextView name;
                public CardView base;

                public viewHolder(View itemView) {
                    super(itemView);
                    name = (TextView) itemView.findViewById(R.id.seasonName);
                    base = (CardView) itemView.findViewById(R.id.seasonCard);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ele_serial_season_clickplay,
                        parent, false);
                return new viewHolder(convertView);
            }

            @Override
            public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

                viewHolder vh = (viewHolder) holder;
                vh.name.setText(obtainedSerial.seasons.get(position).getName());

                vh.base.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(A_SerialDetails.this, A_SerialPlay.class);
                        intent.putExtra("serial", obtainedSerial);
                        intent.putExtra("seasonNumber", holder.getAdapterPosition());
                        startActivity(intent);
                    }
                });
            }

            @Override
            public int getItemCount() {
                return obtainedSerial.seasons.size();
            }
        });
//        for (T_FetchContentVahid.Serial.Season s : obtainedSerial.seasons) {
//            final View a_season = inflater.inflate(R.layout.ele_serial_season_clickplay, parent, false);
//            TextView name = (TextView) a_season.findViewById(R.id.seasonName);
//            name.setText(s.getName());
//
//            final LinearLayout hidden = (LinearLayout) a_season.findViewById(R.id.playSection);
//            TextView countEpi = (TextView) a_season.findViewById(R.id.totalCount);
//            countEpi.setText("/ " + s.episodes.size() + "");
//            Button playEpi = (Button) a_season.findViewById(R.id.playEpisode);
//            EditText episode = (EditText) a_season.findViewById(R.id.playEpiNumber);
//            int epiNumber = 1;
//            try {
//                epiNumber = Integer.parseInt(episode.getText().toString());
//            } catch (Exception ignored) {
//
//            }
//
//            a_season.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    if (hidden.getVisibility() == View.GONE)
//                        hidden.setVisibility(View.VISIBLE);
//                    else
//                        hidden.setVisibility(View.GONE);
//                }
//            });
//            parent.addView(a_season);
//        }
    }

}
