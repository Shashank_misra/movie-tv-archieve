package com.hack.lucifer.movies.DownloadManager;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;

import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.Databases.Download_DB_helper;

public class ReceiverDownloadComplete extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
        Log.d("msg", "in completion broadcast");

        if (reference != -1) {

            SharedPreferences downloadIds = context.getSharedPreferences(Constants.DOWNLOAD_ID_PREFERENCE_FILE, Context.MODE_PRIVATE);
            long statusStored = downloadIds.getLong(reference + "", -1);
            DBManager downDBManager = new DBManager(context);

            if (statusStored != -1) {

                Log.d("msg", "in completion broadcast match found");

                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(reference);
                Cursor cursor = downloadManager.query(query);

                if (cursor.moveToFirst()) {

                    int statusNew = cursor.getInt(
                            cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    String savedFilePath = cursor.getString(
                            cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));

                    Log.d("msg", savedFilePath);
                    Log.d("msg", "completion status " + getMyStateFor(statusNew));

                    if (statusStored != statusNew) {
                        SharedPreferences.Editor edit = downloadIds.edit();
                        edit.remove(reference + "");
                        edit.putLong(reference + "", statusNew);
                        edit.commit();
                    }

                    downDBManager.updateStateForDownloadId(getMyStateFor(statusNew), reference);
                    if (getMyStateFor(statusNew) == Download_DB_helper.STATES.COMPLETE
                            &&
                            savedFilePath.length() > 0) {

                        // update file path to serial db table 'main'
                        String videoId = downDBManager.getVideoIdForDownloadId(reference);
                        if (videoId != null) {

                            com.hack.lucifer.movies.Serials.DBManager serialDBManager = new
                                    com.hack.lucifer.movies.Serials.DBManager(context);
                            serialDBManager.updateOfflineLocation(videoId, savedFilePath);
                        }
                    }
                } else {
                    Log.d("msg", "cursor output false ie. download not found!");

                    // case when download was forcibly removed from manager
                    int result = downDBManager.updateStateForDownloadId(Download_DB_helper.STATES.FAILED, reference);
                    Log.d("msg", "reference: " + reference + "\nupdated " + result + " row");
                    SharedPreferences.Editor edit = downloadIds.edit();
                    edit.remove(reference + "");
                    edit.putLong(reference + "", DownloadManager.STATUS_FAILED);
                    edit.commit();
                }
            }
        } else Log.d("msg", "received intent in receiver was empty!");
    }

    private Download_DB_helper.STATES getMyStateFor(int state_db) {

        switch (state_db) {
            case DownloadManager.STATUS_FAILED:
                return Download_DB_helper.STATES.FAILED;
            case DownloadManager.STATUS_PAUSED:
                return Download_DB_helper.STATES.PAUSED;
            case DownloadManager.STATUS_PENDING:
                return Download_DB_helper.STATES.PENDING;
            case DownloadManager.STATUS_RUNNING:
                return Download_DB_helper.STATES.ONGOING;
            case DownloadManager.STATUS_SUCCESSFUL:
                return Download_DB_helper.STATES.COMPLETE;
        }
        return Download_DB_helper.STATES.UNAVAILABLE;
    }
}
