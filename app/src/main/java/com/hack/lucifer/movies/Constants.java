package com.hack.lucifer.movies;


public interface Constants {

    int ASSUMED_MAX_EPISODES_PER_SEASON = 50;
    String SP_STORAGE_TAG = "storageLocation";
    String SP_STORAGE_VAL_INTERNAL = "internal";
    String SP_STORAGE_VAL_EXTERNAL = "external";

    String DIRECTORY_NAME = "Movies & TV Serials";
    String MOVIE_DATA_FILE_NAME = "movies.ser";
    String TV_DATA_FILE_NAME = "serials.ser";
    String DOWNLOAD_ID_PREFERENCE_FILE = "DownloadIds";

    String BASE_URL_FARSI = "http://dl.farsmovie.ir/movie/";
    String BASE_URL_1327 = "http://217.219.143.108/1327/";
    String BASE_URL_TECHMOVIES = "http://dl.tehmovies.com/94/series/";
    String BASE_URL_98MUSIC = "http://dl2.my98music.com/Data/Serial/";
    String BASE_URL_VAHID = "http://dl.vahidfilm.com/Serial/";

    String OMDB_BASE_URL = "http://www.omdbapi.com/?";

    String BLOBID = "https://jsonblob.com/api/jsonBlob/57a45748e4b0dc55a4eaaa4e";

    String BLOBID_FARSI_EMPTY = "https://jsonblob.com/api/jsonBlob/57a58748e4b0dc55a4eae754";
    String BLOBID_FARSI = "https://jsonblob.com/api/jsonBlob/57e0e4b2e4b0dc55a4f71a43";


    String BLOBID_1327_EMPTY = "https://jsonblob.com/api/jsonBlob/57a5c114e4b0dc55a4eae941";
    String BLOBID_1327 = "https://jsonblob.com/api/jsonBlob/57e0e553e4b0dc55a4f71a6e";

    String BLOBID_TVSERIES_VAHID_EMPTY = "https://jsonblob.com/api/jsonBlob/57a9c6efe4b0dc55a4eb9d36";
    String BLOBID_TVSERIES_VAHID = "https://jsonblob.com/api/jsonBlob/57e0e5b8e4b0dc55a4f71a8a";

    String BLOB_PUT_URL = "https://jsonblob.com/api/jsonBlob/57a45748e4b0dc55a4eaaa4e";

    String BLOB_ANALYTICS = "https://jsonblob.com/api/jsonBlob/57e37e1be4b0dc55a4f7f955";


}
