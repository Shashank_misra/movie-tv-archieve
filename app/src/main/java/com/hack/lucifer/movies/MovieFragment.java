package com.hack.lucifer.movies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hack.lucifer.movies.Movies.FinishedMovie;
import com.hack.lucifer.movies.Movies.MoviePlayActivity;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class MovieFragment extends Fragment {

    public interface MovieCommunicator{
        ArrayList<FinishedMovie> getMovieData();
    }

    MovieCommunicator communicator;
    public mAdapter listAdapter;
    private RecyclerView movieList;
    ArrayList<FinishedMovie> movieData;
    private StaggeredGridLayoutManager mLayoutManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            communicator = (MovieCommunicator) activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString() +
                    "must implement GetDataInterface Interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.f_movies, container, false);
        movieList = (RecyclerView) rootView.findViewById(R.id.list_of_movies);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        movieList.setLayoutManager(mLayoutManager);
        movieList.setItemAnimator(new DefaultItemAnimator());

        final MainActivity activity = (MainActivity) getActivity();
        if (movieData == null && MainActivity.globalReadyMovies != null)
            movieData = MainActivity.globalReadyMovies;

        listAdapter = new mAdapter(movieData, activity);
        movieList.setAdapter(listAdapter);

        movieList.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        movieList.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                        float viewWidth = displayMetrics.widthPixels / displayMetrics.density;
                        float cardViewWidth = 180.0f;
                        int newSpanCount = (int) Math.floor(viewWidth / cardViewWidth);
                        mLayoutManager.setSpanCount(newSpanCount);
                        mLayoutManager.requestLayout();
                    }
                });

        return rootView;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float viewWidth = displayMetrics.widthPixels / displayMetrics.density;
        float cardViewWidth = 180.0f;
        int newSpanCount = (int) Math.floor(viewWidth / cardViewWidth);
        mLayoutManager.setSpanCount(newSpanCount);
        mLayoutManager.requestLayout();
    }

    public class mAdapter extends RecyclerView.Adapter<mAdapter.Holder> {

        Context con;
        List<FinishedMovie> data = new ArrayList<>();
        LayoutInflater inflater;

        mAdapter(List<FinishedMovie> data, Context context) {

            this.data = data;
            con = context;
            inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public mAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = inflater.inflate(R.layout.row_movielist_new, parent, false);
//            itemView.getLayoutParams().width = RecyclerView.LayoutParams.MATCH_PARENT;
//            itemView.getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, con.getResources().getDisplayMetrics());
            return new Holder(itemView);
        }

        @Override
        public void onBindViewHolder(mAdapter.Holder holder, final int position) {

            String iurl = data.get(position).iurl;
            String title = data.get(position).title;
            String dir = data.get(position).director;
            String release = data.get(position).release;
            String imdbR = "IMDB: " + data.get(position).rating;
//            String size = "File Size: " + data.get(position).size + "B";
            String size = data.get(position).size + "B";

            holder.imdbRating.setTypeface(FontUtils.SecfontR);
            holder.release.setTypeface(FontUtils.SecfontR);
            holder.director.setTypeface(FontUtils.SecfontR);
            holder.name.setTypeface(FontUtils.SecfontB);
            holder.size.setTypeface(FontUtils.SecfontB);

            holder.imdbRating.setText(imdbR);
            holder.release.setText(release);
            holder.director.setText(dir);
            holder.name.setText(title);
            holder.size.setText(size);

            try {
//                Log.d("msg", data.get(position).rating + "");
                holder.ratingStars.setMax(5);
                holder.ratingStars.setRating(Float.parseFloat(data.get(position).rating) / 2);
            } catch (Exception ignored) {

            }

            if (data.get(position).director.equalsIgnoreCase("N/A"))
                holder.director.setVisibility(View.GONE);
            else
                holder.director.setVisibility(View.VISIBLE);

            if (data.get(position).release.equalsIgnoreCase("N/A"))
                holder.release.setVisibility(View.GONE);
            else
                holder.release.setVisibility(View.VISIBLE);

            if (data.get(position).rating.equalsIgnoreCase("N/A"))
                holder.imdbRating.setVisibility(View.GONE);
            else
                holder.imdbRating.setVisibility(View.VISIBLE);

            if (data.get(position).size.equalsIgnoreCase("N/A"))
                holder.size.setVisibility(View.GONE);
            else
                holder.size.setVisibility(View.VISIBLE);


            Picasso.with(con).load(iurl).placeholder(R.drawable.placeholder_movie_b).into(holder.poster);
            holder.movieItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinishedMovie finishedMovie = data.get(position);
                    if (Utils.isConnected(getActivity())) {
                        if (finishedMovie.dUrl != null && finishedMovie.dUrl != "") {
                            Intent intent = new Intent(getActivity(), MoviePlayActivity.class);
                            intent.putExtra("movieDataFinal", (Serializable) finishedMovie);
                            getActivity().startActivity(intent);
                        }
                    } else {
                        Snackbar.make(v, "Not connected to Internet", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class Holder extends RecyclerView.ViewHolder {

            ImageView poster;
            TextView name, director, release, imdbRating, size;
            RelativeLayout movieItemLayout;
            RatingBar ratingStars;

            public Holder(View itemView) {
                super(itemView);

                poster = (ImageView) itemView.findViewById(R.id.poster);
                name = (TextView) itemView.findViewById(R.id.title);
                director = (TextView) itemView.findViewById(R.id.director);
                release = (TextView) itemView.findViewById(R.id.release);
                imdbRating = (TextView) itemView.findViewById(R.id.imdbRating);
                ratingStars = (RatingBar) itemView.findViewById(R.id.rating);
                size = (TextView) itemView.findViewById(R.id.size);
                movieItemLayout = (RelativeLayout) itemView.findViewById(R.id.movieItemLayout);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(communicator != null){
            movieData = communicator.getMovieData();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public class GridAutofitLayoutManager extends GridLayoutManager {
        private int mColumnWidth;
        private boolean mColumnWidthChanged = true;

        public GridAutofitLayoutManager(Context context, int columnWidth) {
        /* Initially set spanCount to 1, will be changed automatically later. */
            super(context, 1);
            setColumnWidth(checkedColumnWidth(context, columnWidth));
        }

        public GridAutofitLayoutManager(Context context, int columnWidth, int orientation, boolean reverseLayout) {
        /* Initially set spanCount to 1, will be changed automatically later. */
            super(context, 1, orientation, reverseLayout);
            setColumnWidth(checkedColumnWidth(context, columnWidth));
        }

        private int checkedColumnWidth(Context context, int columnWidth) {
            if (columnWidth <= 0) {
            /* Set default columnWidth value (48dp here). It is better to move this constant
            to static constant on top, but we need context to convert it to dp, so can't really
            do so. */
                columnWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48,
                        context.getResources().getDisplayMetrics());
            }
            return columnWidth;
        }

        public void setColumnWidth(int newColumnWidth) {
            if (newColumnWidth > 0 && newColumnWidth != mColumnWidth) {
                mColumnWidth = newColumnWidth;
                mColumnWidthChanged = true;
            }
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            int width = getWidth();
            int height = getHeight();
            if (mColumnWidthChanged && mColumnWidth > 0 && width > 0 && height > 0) {
                int totalSpace;
                if (getOrientation() == VERTICAL) {
                    totalSpace = width - getPaddingRight() - getPaddingLeft();
                } else {
                    totalSpace = height - getPaddingTop() - getPaddingBottom();
                }
                int spanCount = Math.max(1, totalSpace / mColumnWidth);
                setSpanCount(spanCount);
                mColumnWidthChanged = false;
            }
            super.onLayoutChildren(recycler, state);
        }
    }
}
