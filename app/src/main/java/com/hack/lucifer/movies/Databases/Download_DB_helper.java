package com.hack.lucifer.movies.Databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class Download_DB_helper extends SQLiteOpenHelper {


    public enum STATES {
        FAILED(0), PAUSED(1), PENDING(2), ONGOING(3), COMPLETE(4), UNAVAILABLE(-1);

        public int getVal() {
            return val;
        }

        private final int val;

        STATES(int val) {
            this.val = val;
        }

        public static STATES getState(int state) {

            switch (state) {
                case -1:
                    return UNAVAILABLE;
                case 0:
                    return FAILED;
                case 1:
                    return PAUSED;
                case 2:
                    return PENDING;
                case 3:
                    return ONGOING;
                case 4:
                    return COMPLETE;
            }
            return null;
        }

        @Override
        public String toString() {
            String result = null;
            switch (this.getVal()) {
                case 0:
                    result = "FAILED";
                    break;
                case 1:
                    result = "PAUSED";
                    break;
                case 2:
                    result = "PENDING";
                    break;
                case 3:
                    result = "ONGOING";
                    break;
                case 4:
                    result = "COMPLETE";
                    break;
                case -1:
                    result = "UNAVAILABLE";
                    break;
            }
            return result;
        }
    }

    private static final int DATABASE_VERSION = 68;
    private static final String DATABASE_NAME = "downloads.db";

    public static final String TABLE = "downloads";
    public static final String VIDEO_ID = "video_id";
    public static final String DOWNLOAD_ID = "down_id";
    public static final String STATE = "state";
    public static final String NAME = "video_name";
    public static final String SIZE = "video_download_size";
    public static final String URL = "video_url";
    public static final String SAVE_PATH = "save_path";

    public Download_DB_helper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "CREATE TABLE " + TABLE + " (" +
                        "_ID integer primary key autoincrement, " +
                        VIDEO_ID + " text not null, " +
                        DOWNLOAD_ID + " integer not null, " +
                        STATE + " integer not null, " +
                        SIZE + " text, " +
                        SAVE_PATH + " text, " +
                        URL + " text not null, " +
                        NAME + " text not null ) "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(db);
    }
}
