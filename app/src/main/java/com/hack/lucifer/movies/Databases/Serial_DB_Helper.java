package com.hack.lucifer.movies.Databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class Serial_DB_Helper extends SQLiteOpenHelper {

    public static final String TABLE_SERIALS = "Main";
    public static final String VIDEO_ID = "videoId";
    public static final String FLAG_OFFLINE = "offline";
    public static final String FLAG_WATCHED = "watched";
    public static final String PLAY_TIME = "playedTill";
    public static final String STORE_PATH = "storagePath";
    public static final String SERIAL = "serial";
    public static final String SEASON_NUM = "seasonNumber";
    public static final String EPISODE_NUM = "epiNumber";
    public static final String URL = "url";

    private static final int DATABASE_VERSION = 15;
    private static final String DATABASE_NAME = "serialDb.db";
    private static final String KEY_ID = "_id";


    public Serial_DB_Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(
                "create table " + TABLE_SERIALS + " " +
                        "(" + KEY_ID + " integer primary key autoincrement, " +
                        VIDEO_ID + " text not null, " +
                        FLAG_OFFLINE + " integer not null default 0, " +
                        PLAY_TIME + " integer not null," +
                        STORE_PATH + " text, " +
                        SERIAL + " text not null, " +
                        SEASON_NUM + " integer not null, " +
                        FLAG_WATCHED + " integer not null default 0, " +
                        EPISODE_NUM + " integer not null default -1, " +
                        URL + " text not null )"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.d("msg", "in upgrade");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SERIALS);
        onCreate(db);
    }
}
