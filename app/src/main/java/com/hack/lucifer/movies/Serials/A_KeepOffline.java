package com.hack.lucifer.movies.Serials;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.hack.lucifer.movies.DownloadManager.DownloadActivityV2;
import com.hack.lucifer.movies.R;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;

public class A_KeepOffline extends AppCompatActivity {

    public T_FetchContentVahid.Serial obtainedSerial;
    Button linkDownloadManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_download_serial);

        obtainedSerial = (T_FetchContentVahid.Serial) getIntent().getSerializableExtra("data");
        linkDownloadManager = (Button) findViewById(R.id.sendToManager);
        if (linkDownloadManager != null) {
            linkDownloadManager.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(A_KeepOffline.this, DownloadActivityV2.class);
                    startActivity(intent);
                    DownloadActivityV2.appRunning = true;
                }
            });
        }
        initialize();
    }

    private void initialize() {

        ViewPager seasonPager = (ViewPager) findViewById(R.id.seasonPager);
        if (seasonPager != null) {
            seasonPager.setAdapter(new mAdapter(getSupportFragmentManager(), obtainedSerial));
        }
//        TextView title = (TextView) findViewById(R.id.serialName);
//        if (title != null) {
//            title.setText(obtainedSerial.name);
//        }
    }

    public class mAdapter extends FragmentPagerAdapter {

        T_FetchContentVahid.Serial about;

        public mAdapter(FragmentManager fm, T_FetchContentVahid.Serial serial) {
            super(fm);
            about = serial;
        }

        @Override
        public Fragment getItem(int position) {

            F_EpisodeList_Download fragment = new F_EpisodeList_Download();
            Bundle data = new Bundle();
            data.putSerializable("data", about.seasons.get(position));
            fragment.setArguments(data);

            return fragment;
        }

        @Override
        public int getCount() {
            return about.seasons.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return about.seasons.get(position).getName();
        }
    }
}
