package com.hack.lucifer.movies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hack.lucifer.movies.Serials.A_SerialDetails;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;


public class TVSeriesFragment extends Fragment {

    public interface TVCommunicator{
        ArrayList<T_FetchContentVahid.Serial> getSerialData();
    }

    TVCommunicator communicator;
    private RecyclerView seriesList;
    private mAdapter adapter;
    MainActivity activity;

    public static ArrayList<T_FetchContentVahid.Serial> serialData;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            communicator = (TVCommunicator) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString() +
                    "must implement GetDataInterface Interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.f_tvseries, container, false);

        activity = (MainActivity) getActivity();
        seriesList = (RecyclerView) rootView.findViewById(R.id.seriesList);
//        Bundle data = getArguments();
//        if (data.getSerializable("data") != null)
//            serialData = (ArrayList<T_FetchContentVahid.Serial>) data.getSerializable("data");

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(communicator != null){
            serialData = communicator.getSerialData();
        }

        adapter = new mAdapter(activity, serialData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        seriesList.setLayoutManager(mLayoutManager);
        seriesList.setItemAnimator(new DefaultItemAnimator());
        seriesList.setAdapter(adapter);
    }

    class mAdapter extends RecyclerView.Adapter<mAdapter.Holder> {

        Context context;
        List<T_FetchContentVahid.Serial> data;
        Vibrator vibrator;
        Bitmap blurredBitmaps[];

        mAdapter(Context context, List<T_FetchContentVahid.Serial> data) {
            this.context = context;
            this.data = data;
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

            if (data != null)
                blurredBitmaps = new Bitmap[this.data.size()];
        }

        public class Holder extends RecyclerView.ViewHolder {

            public TextView name, plot, ratingNum;
            public ImageView poster, posterBlur;
            public RatingBar ratingStars;
            public View container;

            public Holder(View itemView) {
                super(itemView);
                container = itemView;
                name = (TextView) itemView.findViewById(R.id.name);
                plot = (TextView) itemView.findViewById(R.id.plot);
                ratingNum = (TextView) itemView.findViewById(R.id.ratingNum);
                ratingStars = (RatingBar) itemView.findViewById(R.id.rating);
                poster = (ImageView) itemView.findViewById(R.id.poster);
                posterBlur = (ImageView) itemView.findViewById(R.id.posterBlur);
            }
        }

        @Override
        public mAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_grp_serieslist_d, parent, false);
            return new Holder(itemView);
        }

        @Override
        public void onBindViewHolder(final mAdapter.Holder holder, final int position) {

            if (blurredBitmaps == null || blurredBitmaps.length == 0)
                blurredBitmaps = new Bitmap[getItemCount()];

            final T_FetchContentVahid.Serial serial = data.get(position);

            holder.name.setText(serial.name);
            holder.plot.setText(serial.description);
            holder.ratingNum.setText(serial.rating);
            try {
//                Log.d("msg", serial.rating + "");
                holder.ratingStars.setMax(5);
                holder.ratingStars.setRating(Float.parseFloat(serial.rating) / 2);
            } catch (Exception ignored) {

            }

            final Bitmap[] downloadedImage = new Bitmap[1];
            Utils.imageLoader.displayImage(serial.iUrl, holder.poster, Utils.options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {

                            if (bitmap != null) {
                                holder.poster.setVisibility(View.VISIBLE);
                                holder.poster.setImageBitmap(bitmap);
                                holder.posterBlur.setImageBitmap(bitmap);
                                downloadedImage[0] = bitmap;
                            } else {
                                holder.poster.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    }
            );

            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getActivity(), A_SerialDetails.class);
                    intent.putExtra("serial", serial);
                    AppController.b1 = downloadedImage[0];
                    AppController.b2 = downloadedImage[0];
                    A_SerialDetails.bitmapAvailable = true;
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
//    public class m2Adapter extends BaseAdapter {
//
//        Context context;
//        List<T_FetchContentVahid.Serial> data;
//        Vibrator vibrator;
//        Bitmap blurredBitmaps[];
//
//        m2Adapter(Context context, List<T_FetchContentVahid.Serial> data) {
//            this.context = context;
//            this.data = data;
//            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
//            blurredBitmaps = new Bitmap[this.data.size()];
//        }
//
//        @Override
//        public int getCount() {
//            return data.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return data.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//
//            if (blurredBitmaps == null || blurredBitmaps.length == 0)
//                blurredBitmaps = new Bitmap[getCount()];
//
//            final T_FetchContentVahid.Serial serial = data.get(position);
//
//            if (convertView == null) {
//                LayoutInflater inflater = (LayoutInflater) this.context
//                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                convertView = inflater.inflate(R.layout.row_grp_serieslist_d, parent, false);
//            }
//
//            TextView name = (TextView) convertView.findViewById(R.id.name);
//            TextView plot = (TextView) convertView.findViewById(R.id.plot);
//            TextView ratingNum = (TextView) convertView.findViewById(R.id.ratingNum);
//            final RatingBar ratingStars = (RatingBar) convertView.findViewById(R.id.rating);
//            final ImageView poster = (ImageView) convertView.findViewById(R.id.poster);
//            final ImageView posterBlur = (ImageView) convertView.findViewById(R.id.posterBlur);
//
//            name.setText(serial.name);
//            plot.setText(serial.description);
//            ratingNum.setText(serial.rating);
//            try {
////                Log.d("msg", serial.rating + "");
//                ratingStars.setMax(5);
//                ratingStars.setRating(Float.parseFloat(serial.rating) / 2);
//            } catch (Exception ignored) {
//
//            }
//
//            final Bitmap[] downloadedImage = {null};
//            Utils.imageLoader.displayImage(serial.iUrl, poster, Utils.options, new ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String s, View view) {
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String s, View view, FailReason failReason) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
//
//                            downloadedImage[0] = bitmap;
//                            if (blurredBitmaps[position] == null) {
//                                if (bitmap != null) {
//                                    Bitmap blurred = Utils.blurRenderScript(context, bitmap, 25);
//                                    blurredBitmaps[position] = blurred;
//                                    poster.setVisibility(View.VISIBLE);
//                                    poster.setImageBitmap(bitmap);
//                                    posterBlur.setImageBitmap(blurred);
//                                } else {
//                                    poster.setVisibility(View.GONE);
//                                    Bitmap blurred = Utils.blurRenderScript(context, BitmapFactory.decodeResource(getResources(), R.drawable.tv_placeholder), 25);
//                                    posterBlur.setImageBitmap(blurred);
//                                    blurredBitmaps[position] = blurred;
//                                }
//                            } else {
//                                if (bitmap != null) {
//                                    poster.setVisibility(View.VISIBLE);
//                                    poster.setImageBitmap(bitmap);
//                                    posterBlur.setImageBitmap(blurredBitmaps[position]);
//                                } else {
//                                    posterBlur.setImageBitmap(blurredBitmaps[position]);
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String s, View view) {
//
//                        }
//                    }
//            );
//
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(getActivity(), A_SerialDetails.class);
//                    intent.putExtra("serial", serial);
//                    AppController.b1 = blurredBitmaps[position];
//                    AppController.b2 = downloadedImage[0];
//                    startActivity(intent);
//                }
//            });
//            return convertView;
//        }
//    }

//    public class mAdapter extends BaseExpandableListAdapter {
//
//        Context context;
//        List<T_FetchContentVahid.Serial> data;
//        Vibrator vibrator;
//        Bitmap blurredBitmaps[];
//
//        mAdapter(Context context, List<T_FetchContentVahid.Serial> data) {
//            this.context = context;
//            this.data = data;
//            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
//            blurredBitmaps = new Bitmap[data.size()];
//        }
//
//        @Override
//        public int getGroupCount() {
//            return data.size();
//        }
//
//        @Override
//        public int getChildrenCount(int groupPosition) {
//            return data.get(groupPosition).seasons.size();
//        }
//
//        @Override
//        public Object getGroup(int groupPosition) {
//            return data.get(groupPosition);
//        }
//
//        @Override
//        public Object getChild(int groupPosition, int childPosition) {
//            return data.get(groupPosition).seasons.get(childPosition);
//        }
//
//        @Override
//        public long getGroupId(int i) {
//            return i;
//        }
//
//        @Override
//        public long getChildId(int i, int i1) {
//            return i1;
//        }
//
//        @Override
//        public boolean hasStableIds() {
//            return false;
//        }
//
//        @Override
//        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
//
//            if (blurredBitmaps == null || blurredBitmaps.length == 0)
//                blurredBitmaps = new Bitmap[getGroupCount()];
//            T_FetchContentVahid.Serial serial = (T_FetchContentVahid.Serial) getGroup(groupPosition);
//
//            if (convertView == null) {
//                LayoutInflater inflater = (LayoutInflater) this.context
//                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                convertView = inflater.inflate(R.layout.row_grp_serieslist_d, parent, false);
//            }
//
//            TextView name = (TextView) convertView.findViewById(R.id.name);
//            TextView plot = (TextView) convertView.findViewById(R.id.plot);
//            TextView ratingNum = (TextView) convertView.findViewById(R.id.ratingNum);
//            RatingBar ratingStars = (RatingBar) convertView.findViewById(R.id.rating);
//            final ImageView poster = (ImageView) convertView.findViewById(R.id.poster);
//            final ImageView posterBlur = (ImageView) convertView.findViewById(R.id.posterBlur);
//
//            name.setText(serial.name);
//            plot.setText(serial.description);
//            ratingNum.setText(serial.rating);
//            try {
//                Log.d("msg", serial.rating + "");
//                ratingStars.setMax(5);
//                ratingStars.setRating(Float.parseFloat(serial.rating) / 2);
//            } catch (Exception ignored) {
//
//            }
//            Utils.imageLoader.displayImage(serial.iUrl, poster, Utils.options, new ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String s, View view) {
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String s, View view, FailReason failReason) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
//
//                            if (blurredBitmaps[groupPosition] == null) {
//                                if (bitmap != null) {
//                                    Bitmap blurred = Utils.blurRenderScript(context, bitmap, 25);
//                                    blurredBitmaps[groupPosition] = blurred;
//                                    poster.setVisibility(View.VISIBLE);
//                                    poster.setImageBitmap(bitmap);
//                                    posterBlur.setImageBitmap(blurred);
//                                } else {
//                                    poster.setVisibility(View.GONE);
//                                    Bitmap blurred = Utils.blurRenderScript(context, BitmapFactory.decodeResource(getResources(), R.drawable.tv_placeholder), 25);
//                                    posterBlur.setImageBitmap(blurred);
//                                    blurredBitmaps[groupPosition] = blurred;
//                                }
//                            } else {
//                                if (bitmap != null) {
//                                    poster.setVisibility(View.VISIBLE);
//                                    poster.setImageBitmap(bitmap);
//                                    posterBlur.setImageBitmap(blurredBitmaps[groupPosition]);
//                                } else {
//                                    posterBlur.setImageBitmap(blurredBitmaps[groupPosition]);
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String s, View view) {
//
//                        }
//                    }
//            );
//            return convertView;
//        }
//
//        @Override
//        public View getChildView(final int groupPostion, int childPosition,
//                                 boolean isLastChild, View convertView, ViewGroup parent) {
//
//            final T_FetchContentVahid.Serial.Season season = (T_FetchContentVahid.Serial.Season) getChild(groupPostion, childPosition);
//
//            if (convertView == null) {
//                LayoutInflater infalInflater = (LayoutInflater) this.context
//                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                convertView = infalInflater.inflate(R.layout.row_item_serieslist, parent, false);
//            }
//
//            LinearLayout container = (LinearLayout) convertView.findViewById(R.id.container);
//            TextView seasonName = (TextView) convertView
//                    .findViewById(R.id.seasonName);
//            TextView episodeCount = (TextView) convertView.
//                    findViewById(R.id.epiCount);
//
//            seasonName.setText(season.getName());
//            episodeCount.setText("Episodes " + season.episodes.size());
//
//            container.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
////                    Bundle b = new Bundle();
////                    b.putSerializable("season", season);
////                    startActivity(new Intent(getActivity(), A_SerialPlay.class).putExtra("data", b));
//                    Intent intent = new Intent(getActivity(), A_SerialPlay.class);
//                    intent.putExtra("season", season);
//                    intent.putExtra("image", data.get(groupPostion).iUrl);
//                    startActivity(intent);
//
//                }
//            });
//            return convertView;
//        }
//
//        @Override
//        public boolean isChildSelectable(int i, int i1) {
//            return true;
//        }
//    }
}
