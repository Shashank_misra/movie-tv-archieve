package com.hack.lucifer.movies.MovieServers;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hack.lucifer.movies.*;
import com.hack.lucifer.movies.Movies.FinishedMovie;
import com.hack.lucifer.movies.TVSeriesServers.T_CheckContentTVSeries;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class M_FetchContent1327 extends AsyncTask<Void, String, ArrayList<FinishedMovie>> {

    String queryURL, serverName;
    SplashActivity callingClass;

    public M_FetchContent1327(Activity activity, String serverURL, String servername) {

        callingClass = (SplashActivity) activity;
        queryURL = serverURL;
        this.serverName = servername;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        callingClass.setUpdateMessage("Connecting to Server 2");
    }

    @Override
    protected ArrayList<FinishedMovie> doInBackground(Void... voids) {

        /*
        Fetch All Content Links
         */
        Document document = null;
        ArrayList<temp> data = new ArrayList<>();
        try {
            document = Jsoup.connect(queryURL).timeout(60000).get();
            Elements links = document.select("a[href]");


            for (Element link : links) {

                String name = link.text();
                String url = link.attr("abs:href");
                if (!name.startsWith("-") && !name.equals("../")) {
                    name = name.replaceAll("(\\d{4})", "");
                    int i = name.indexOf("()");
                    if (i != -1)
                        name = name.substring(0, i);
                    name = name.trim();
                    data.add(new temp(name, url));
                }
            }
            publishProgress("Found " + data.size() + " links");
        } catch (IOException e) {
            e.printStackTrace();
        }
//        assert document != null;


        /*
        Fetching all the download links
         */
        int movieCount = 0;
        final ArrayList<MainActivity.rawMovie> rawMovies = new ArrayList<>();

        for (temp obj : data) {

            try {
                document = Jsoup.connect(obj.urlForDir).timeout(30000).get();
                Elements links = document.select("a[href]");

                for (Element link : links) {

                    String text = link.text();
                    if (!text.equals("../")) {

                        obj.fileUrl = link.attr("abs:href");
//                    //finding sze
//                    Connection.Response html;
//                    try {
//                        html = Jsoup.connect(obj.urlForDir).execute();
//                        Log.d("msg", "html: " + html.body());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    String htmlPage = document.outerHtml();
//                    String p[] = htmlPage.split("\n");
//                    String size = null;
//                    if (p.length > 6) {
//                        String p2[] = p[6].split(" ");
//                        size = p2[p2.length - 1];
//                    }
                        MainActivity.rawMovie item = new MainActivity.rawMovie();
                        item.name = obj.name;
                        item.url = obj.fileUrl;
                        item.size = "n/a";
                        rawMovies.add(item);
                        movieCount++;

                        Log.d("msg", "\nname: " + obj.name + "\nurl: " + obj.fileUrl + "\nsz: ");
                    }
                    publishProgress("Downloadable: " + movieCount);
                }
            } catch (IOException e) {
                e.printStackTrace();
                publishProgress("_");
            }
        }

        int currCapacity = callingClass.movieDataRaw.size();
        callingClass.movieDataRaw.addAll(rawMovies);
        publishProgress("Fetching Metadata");

        /*
        Fetching Metadata for all movies
         */
        final int[] count = {0};
        int lCount = 1;
        int currReadyListSize = callingClass.movieDataFinal.size();
        Log.d("m_name", (callingClass.movieDataRaw.size() - currCapacity) + " movies to go for metadata");

        for (int j = currCapacity; j < callingClass.movieDataRaw.size(); j++) {

            MainActivity.rawMovie rm = callingClass.movieDataRaw.get(j);
            try {
                String query_url = Constants.OMDB_BASE_URL +
                        "t=" + rm.name;
                if (rm.yr != null)
                    query_url += "&y=" + rm.yr;
                query_url += "&plot=full&r=json";
                Log.d("msg", lCount++ + ". " + rm.name);

                JSONObject response = com.hack.lucifer.movies.Utils.getJSONObjectFromUrl(query_url);
                Log.d("msg", "returned with: " + response);
                if (response != null) {
                    FinishedMovie result = Utils.parseOMDBDataForMovie(response.toString(), rm.url, rm.size, rm.name);
                    if (result != null) {
                        callingClass.movieDataFinal.add(result);
                        count[0]++;
                    }
                    publishProgress("Metadata found: " + count[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        publishProgress("Caching data to local Server");
        JsonObjectRequest putOnServer = new JsonObjectRequest(Request.Method.PUT,
                Constants.BLOBID_1327,
                Utils.getTemplateJsonForMovie(callingClass.movieDataFinal, new String[]{serverName}, new String[]{queryURL}, currReadyListSize),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callingClass.setUpdateMessage("Successfully updated data to Server");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callingClass.setUpdateMessage("ERROR updating data to server");
            }
        });
        AppController.getInstance().addToRequestQueue(putOnServer);
        return callingClass.movieDataFinal;
    }

    @Override
    protected void onProgressUpdate(String... values) {

        if (!values[0].equals("_")) {
            callingClass.setUpdateMessage(values[0]);
            Toast.makeText(callingClass, "CRASHED", Toast.LENGTH_SHORT).show();

        } else
            callingClass.setUpdateMessage(values[0]);
    }

    @Override
    protected void onPostExecute(ArrayList<FinishedMovie> finishedMovies) {
        super.onPostExecute(finishedMovies);
//        if (MovieFragment.movieFragment != null) {
//            MovieFragment.movieFragment.listAdapter.notifyDataSetChanged();
//        }

        if (SplashActivity.goForSerialServers) {
            T_CheckContentTVSeries check = new T_CheckContentTVSeries(callingClass);
            check.execute();
        }
    }

    class temp {
        String name, urlForDir, fileUrl;

        public temp(String name, String url) {
            this.name = name;
            this.urlForDir = url;
        }
    }
}
