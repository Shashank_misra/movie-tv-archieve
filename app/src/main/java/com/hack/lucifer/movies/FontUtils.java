package com.hack.lucifer.movies;

import android.graphics.Typeface;

/**
 * Created by varnit_khandelwal on 06/08/16.
 *
 * @company SilverPush
 */
public class FontUtils {

    public static Typeface SecfontB = Typeface.createFromAsset(AppController.getInstance().getAssets(), "fonts/cuyabra_bold.otf");
    public static Typeface SecfontR = Typeface.createFromAsset(AppController.getInstance().getAssets(), "fonts/cuyabra.otf");
//    static Typeface OpenSansR = Typeface.createFromAsset(AppController.getInstance().getAssets(), "fonts/OpenSans-Regular.tff");
//    static Typeface OpenSansB = Typeface.createFromAsset(AppController.getInstance().getAssets(), "fonts/OpenSans-Semibold.ttf");

    public static Typeface splashTitle = Typeface.createFromAsset(AppController.getInstance().getAssets(), "fonts/madisi.ttf");
}
