package com.hack.lucifer.movies.DownloadManager;


import android.app.Activity;
import android.app.DownloadManager;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.Databases.Download_DB_helper;
import com.hack.lucifer.movies.R;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class F_DownloadedItemList extends Fragment {

    Communicator comms;
    int position;
    ArrayList<DownloadActivityV2.dataPacket> itemList = new ArrayList<>();
    adapter mAdapter;
    DownloadManager downloadManager;
    DBManager downDBManager;
    com.hack.lucifer.movies.Serials.DBManager serialDBManager;

    RecyclerView downloadList;
    TextView emptyTag;

    public interface Communicator {
        ArrayList<DownloadActivityV2.dataPacket> getItemList(int position);
    }

    public F_DownloadedItemList() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt("ARG");
        }
        downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        downDBManager = new DBManager(getActivity());
        serialDBManager = new com.hack.lucifer.movies.Serials.DBManager(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();

        if (comms != null)
            itemList = comms.getItemList(position);

        mAdapter = new adapter(itemList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        downloadList.setLayoutManager(mLayoutManager);
        downloadList.setItemAnimator(new DefaultItemAnimator());
        downloadList.setAdapter(mAdapter);

        if (itemList.size() == 0) {
            downloadList.setVisibility(View.GONE);
            emptyTag.setVisibility(View.VISIBLE);
        } else {
            downloadList.setVisibility(View.VISIBLE);
            emptyTag.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        comms = (Communicator) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_f_downloaded_item_list, container, false);
        downloadList = (RecyclerView) rootView.findViewById(R.id.downloadList);
        emptyTag = (TextView) rootView.findViewById(R.id.emptyTag);
        return rootView;
    }

    public static F_DownloadedItemList getInstance(int position) {

        F_DownloadedItemList fragment = new F_DownloadedItemList();
        Bundle args = new Bundle();
        args.putInt("ARG", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        comms = null;
    }

    class adapter extends RecyclerView.Adapter<adapter.mHolder> {

        ArrayList<DownloadActivityV2.dataPacket> data;
        DownloadActivityV2 activity;

        adapter(ArrayList<DownloadActivityV2.dataPacket> data, Context context) {
            this.data = data;
            this.activity = (DownloadActivityV2) context;
        }

        @Override
        public mHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_doanload_list, parent, false);
            return new mHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final mHolder holder, int position) {

            final SharedPreferences setting = activity.getSharedPreferences(Constants.DOWNLOAD_ID_PREFERENCE_FILE,
                    Context.MODE_APPEND);
            final DownloadActivityV2.dataPacket dataP = data.get(position);
            holder.title.setText(dataP.name);
            Log.d("states", dataP.state.toString());

            if (dataP.state == Download_DB_helper.STATES.ONGOING ||
                    dataP.state == Download_DB_helper.STATES.PENDING ||
                    dataP.state == Download_DB_helper.STATES.PAUSED) {

                holder.restart.setEnabled(false);
                holder.restart.setImageResource(R.drawable.ic_restart_green_disabled_24dp);
                holder.stop.setEnabled(true);
                holder.stop.setImageResource(R.drawable.ic_clear_green_enabled_24dp);
                new Thread(new Runnable() {
                    {

                    }

                    @Override
                    public void run() {

                        boolean downloading = true;
                        final boolean[] updatedSize = {false};

                        while (downloading) {

                            DownloadManager.Query q = new DownloadManager.Query();
                            q.setFilterById(dataP.downloadId);

                            Cursor cursor = downloadManager.query(q);
                            if (cursor.moveToFirst()) {
                                float bytes_downloaded = cursor.getInt(cursor
                                        .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                                float bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                                final int dl_progress = (int) ((bytes_downloaded * 100L) / bytes_total);

                                String unit_t = "B";
                                if (bytes_total > 1024 + 100) {

                                    bytes_total = bytes_total / 1024;
                                    unit_t = "KB";
                                    if (bytes_total > 1024 + 100) {

                                        bytes_total = bytes_total / 1024;
                                        unit_t = "MB";
                                        if (bytes_total > 1024 + 100) {

                                            bytes_total = bytes_total / 1024;
                                            unit_t = "GB";
                                        }
                                    }
                                }

                                String unit_d = "B";
                                if (bytes_downloaded > 1024 + 100) {

                                    bytes_downloaded = bytes_downloaded / 1024;
                                    unit_d = "KB";
                                    if (bytes_downloaded > 1024 + 100) {

                                        bytes_downloaded = bytes_downloaded / 1024;
                                        unit_d = "MB";
                                        if (bytes_downloaded > 1024 + 100) {

                                            bytes_downloaded = bytes_downloaded / 1024;
                                            unit_d = "GB";
                                        }
                                    }
                                }

                                final String total = new DecimalFormat("#.##").format(bytes_total) + unit_t;
                                final String complete = new DecimalFormat("#.##").format(bytes_downloaded) + unit_d;

                                /*
                                update total size at-least once when download ongoing
                                 */
                                if (dl_progress > 2)
                                    if (!updatedSize[0]) {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                String size = downDBManager.getVideoSizeForDownloadID(dataP.downloadId);
                                                if (size.equalsIgnoreCase("-1B")) {
                                                    downDBManager.updateSizeForDownloadId(size, dataP.downloadId);
                                                    updatedSize[0] = true;
                                                }
                                            }
                                        });
                                    }

                                /*
                                update visible list on download complete
                                 */
                                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                                    downloading = false;
                                    dataP.state = Download_DB_helper.STATES.COMPLETE;
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            holder.stop.setEnabled(false);
                                            notifyDataSetChanged();
                                        }
                                    });
                                }

                                holder.progressBar.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        holder.progressBar.setProgress(dl_progress);
                                        holder.downloaded.setText(complete);
                                        holder.total.setText(total);
                                    }
                                });

                                cursor.close();
                            } else {
                                downloading = false;
                                Log.d("msg", "updating reference " + dataP.downloadId + " with failed");
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        downDBManager.updateStateForDownloadId(Download_DB_helper.STATES.FAILED, dataP.downloadId);
                                    }
                                });
                                dataP.state = Download_DB_helper.STATES.FAILED;
                            }
                        }
                    }
                }).start();
            } else if (dataP.state == Download_DB_helper.STATES.COMPLETE) {

                holder.outcome.setVisibility(View.VISIBLE);
                holder.outcome.setText("Complete");
                holder.outcome.setTextColor(Color.GREEN);
                holder.progressBar.setProgress(holder.progressBar.getMax());
                holder.downloaded.setVisibility(View.GONE);

                holder.total.setText(downDBManager.getVideoSizeForDownloadID(dataP.downloadId));
                holder.stop.setEnabled(false);
                holder.stop.setImageResource(R.drawable.ic_clear_green_disabled_24dp);
                holder.restart.setEnabled(false);
                holder.restart.setImageResource(R.drawable.ic_restart_green_disabled_24dp);

            } else if (dataP.state == Download_DB_helper.STATES.FAILED ||
                    dataP.state == Download_DB_helper.STATES.UNAVAILABLE) {

                holder.outcome.setVisibility(View.VISIBLE);
                holder.outcome.setText("Failed");
                holder.outcome.setTextColor(Color.RED);
                holder.progressBar.setProgress(0);
                holder.total.setText(downDBManager.getVideoSizeForDownloadID(dataP.downloadId));
                holder.stop.setEnabled(false);
                holder.stop.setImageResource(R.drawable.ic_clear_green_disabled_24dp);
                holder.restart.setEnabled(true);
                holder.restart.setImageResource(R.drawable.ic_restart_green_enabled_24dp);
            }

            holder.restart.setOnClickListener(new View.OnClickListener() {
                {

                }

                @Override
                public void onClick(View view) {

                    AppController.feedback();
                    final long downloadId = addVideoToDownloadManager(dataP.downloadId, dataP.name);
                    if (downloadId != -1) {
                        holder.stop.setEnabled(true);
                        holder.stop.setImageResource(R.drawable.ic_clear_green_enabled_24dp);
                        holder.restart.setEnabled(false);
                        holder.restart.setImageResource(R.drawable.ic_restart_green_disabled_24dp);
                        holder.outcome.setVisibility(View.GONE);

                        /*
                        Determining state of download just added: ONGOING or PENDING
                         */
                        DownloadManager.Query q = new DownloadManager.Query();
                        q.setFilterById(downloadId);
                        Cursor cursor = downloadManager.query(q);
                        cursor.moveToFirst();
                        int state_db = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                        Download_DB_helper.STATES state = getStateFor(state_db);

                        /*
                        update shared preference state
                         */
                        final SharedPreferences.Editor edit = setting.edit();
                        edit.remove(downloadId + "");
                        edit.putLong(downloadId + "", state_db);
                        edit.commit();

                        /*
                        update downloads database
                         */
                        int result = downDBManager.updateRestartedDownloadId(downloadId, dataP.videoId, state);
                        dataP.downloadId = downloadId;
                        dataP.state = state;
                        notifyDataSetChanged();
                        Log.d("msg", "restarted id: " + result + "");
                    } else {
                        Toast.makeText(activity,
                                "Something went wrong!\n Try restarting download from details page",
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
            holder.stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AppController.feedback();
                    downloadManager.remove(dataP.downloadId);
                    downDBManager.updateStateForDownloadId(Download_DB_helper.STATES.FAILED, dataP.downloadId);
                    holder.restart.setEnabled(true);
                    holder.restart.setImageResource(R.drawable.ic_restart_green_enabled_24dp);
                    holder.downloaded.setText("0B");
                    holder.progressBar.setProgress(0);
                    dataP.state = Download_DB_helper.STATES.FAILED;
                    notifyDataSetChanged();
                    holder.stop.setEnabled(false);
                    holder.stop.setImageResource(R.drawable.ic_clear_green_disabled_24dp);
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        private Download_DB_helper.STATES getStateFor(int state_db) {

            switch (state_db) {
                case DownloadManager.STATUS_FAILED:
                    return Download_DB_helper.STATES.FAILED;
                case DownloadManager.STATUS_PAUSED:
                    return Download_DB_helper.STATES.PAUSED;
                case DownloadManager.STATUS_PENDING:
                    return Download_DB_helper.STATES.PENDING;
                case DownloadManager.STATUS_RUNNING:
                    return Download_DB_helper.STATES.ONGOING;
            }
            return null;
        }

        private long addVideoToDownloadManager(long downId, String name) {

            String videoId = downDBManager.getVideoIdForDownloadId(downId);
            Log.d("msg", "querying url for video id: " + videoId);
            String url = serialDBManager.getVideoURL(videoId);
            if (url.length() > 0) {
                Uri Download_Uri = Uri.parse(url);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE);
                request.setTitle(name);
                request.setDescription("Movie TV");
                request.allowScanningByMediaScanner();

                String filePath = downDBManager.getFilePathForDownId(downId);
                if (filePath == null) {
                    Log.d("msg", "path not found in DB");
                    return -1;
                }

//                request.setDestinationInExternalFilesDir(activity, Environment.DIRECTORY_DOWNLOADS, name);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                        filePath);
                return downloadManager.enqueue(request);
            } else {
                Log.d("msg", "url length was zero");
                return -1;
            }
        }

        public class mHolder extends RecyclerView.ViewHolder {

            public TextView title, downloaded, total, outcome;
            public ProgressBar progressBar;
            public ImageButton stop, restart;
            public boolean restartedDownload = false;

            public mHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.identificationTitle);
                downloaded = (TextView) itemView.findViewById(R.id.downloadedSize);
                total = (TextView) itemView.findViewById(R.id.TotalSize);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progress);
                outcome = (TextView) itemView.findViewById(R.id.outCome);
                stop = (ImageButton) itemView.findViewById(R.id.stop);
                restart = (ImageButton) itemView.findViewById(R.id.restart);
            }
        }

    }
}
