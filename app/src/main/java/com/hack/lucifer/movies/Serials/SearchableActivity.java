package com.hack.lucifer.movies.Serials;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hack.lucifer.movies.Databases.Serial_DB_Helper;
import com.hack.lucifer.movies.MainActivity;
import com.hack.lucifer.movies.R;

import java.io.Serializable;
import java.util.ArrayList;

public class SearchableActivity extends ListActivity {

    public static final String FRAGMENT = "sentFromFragment";
    RelativeLayout container1, container2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.a_serial_searchable);

        int from = -1;
        Bundle appData = getIntent().getBundleExtra(SearchManager.APP_DATA);
        if (appData != null) {
            from = appData.getInt(SearchableActivity.FRAGMENT);
        }

        initialize(from);

        this.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                String name = (String) SearchableActivity.this.getListAdapter().getItem(position);
                A_SerialDetails.bitmapAvailable = false;
                Intent intent1 = new Intent(SearchableActivity.this, A_SerialDetails.class);
                intent1.putExtra("serial", (Serializable) MainActivity.serialMap.get(name));
                startActivity(intent1);
            }
        });
    }

    private void initialize(int from) {

        ImageView topLeftBack = (ImageView) findViewById(R.id.back);
        topLeftBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        EditText queryBox = (EditText) findViewById(R.id.displayQuery);
        container1 = (RelativeLayout) findViewById(R.id.c2);
        container2 = (RelativeLayout) findViewById(R.id.containerInternal);


        queryBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                doMySearch(charSequence.toString(), 1);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Intent intent = getIntent();

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            String query = intent.getStringExtra(SearchManager.QUERY);
            queryBox.setText(query);
            doMySearch(query, from);
        } else if (Intent.ACTION_VIEW.equalsIgnoreCase(intent.getAction())) {

            Uri data = intent.getData();
            A_SerialDetails.bitmapAvailable = false;
            Intent intent1 = new Intent(this, A_SerialDetails.class);
            intent1.putExtra("serial", (Serializable) MainActivity.serialMap.get(data.getLastPathSegment()));
            startActivity(intent1);
            this.finish();
        }
    }

    private void doMySearch(String query, int from) {

        // called from serial fragments
        if (from == 1) {
            Serial_DB_Helper helper = new Serial_DB_Helper(this);
            SQLiteDatabase db = helper.getReadableDatabase();

            Cursor mCursor = db.rawQuery("SELECT distinct " + Serial_DB_Helper.SERIAL + " from " + Serial_DB_Helper.TABLE_SERIALS
                    + " where " + Serial_DB_Helper.SERIAL + " like '%" + query + "%'", new String[]{});
            ArrayList<String> data = new ArrayList<>();

            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
                data.add(mCursor.getString(0));
            }

            if (data.size() > 0) {
                container2.setVisibility(View.GONE);
                container1.setVisibility(View.VISIBLE);
                setListAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data));
            } else {
                container2.setVisibility(View.VISIBLE);
                container1.setVisibility(View.GONE);
            }
            mCursor.close();
            db.close();
        }
    }
}
