package com.hack.lucifer.movies;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hack.lucifer.movies.Databases.DatabaseUpdateTask;
import com.hack.lucifer.movies.Databases.Download_DB_helper;
import com.hack.lucifer.movies.DownloadManager.DBManager;
import com.hack.lucifer.movies.DownloadManager.DownloadActivityV2;
import com.hack.lucifer.movies.Movies.FinishedMovie;
import com.hack.lucifer.movies.Serials.SearchableActivity;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements
        MovieFragment.MovieCommunicator,
        TVSeriesFragment.TVCommunicator {

    public static HashMap<String, T_FetchContentVahid.Serial> serialMap = new HashMap<>();
    public static HashMap<String, FinishedMovie> movieMap = new HashMap<>();
    public static boolean mapsAreReady = false;
    public static boolean databaseIsUpdated = false;

    public static ArrayList<FinishedMovie> globalReadyMovies = new ArrayList<>();
    public static ArrayList<T_FetchContentVahid.Serial> globalReadySerials = new ArrayList<>();
    public static String movieFilePath, serialFilePath;
    public FrameLayout updateContainer;
    public ProgressBar databaseUpdateProgress;
    SearchView searchView;
    TextView updateStatus;
    private ViewPager mViewPager;

    DBManager downDBManager;
    com.hack.lucifer.movies.Serials.DBManager serialDBManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        if (!databaseIsUpdated) {
            DatabaseUpdateTask task = new DatabaseUpdateTask(this);
            task.execute();
        } else updateContainer.setVisibility(View.GONE);

        if (databaseIsUpdated) {
            Thread verifyDownloadedVideos = new Thread(new Runnable() {
                @Override
                public void run() {

                    downDBManager = new DBManager(MainActivity.this);
                    serialDBManager = new com.hack.lucifer.movies.Serials.DBManager(MainActivity.this);
                    String response[][] = serialDBManager.getStoragePathsWithVideoID();
                    if (response != null)
                        for (String[] aResponse : response) {
                            File file = new File(aResponse[0]);
                            if (!file.exists()) {
                                if (downDBManager.getVideoStateForVideoID(aResponse[1]).getVal() ==
                                        Download_DB_helper.STATES.COMPLETE.getVal())
                                    downDBManager.removeDownloadForVideoID(aResponse[1]);
                                serialDBManager.putNullToStorePath(aResponse[1]);
                            }
                        }
                }
            });
            verifyDownloadedVideos.start();
        }
    }

    private void showEditLocationDialog() {

        /*
        Code for creating border of selected item
         */
        final GradientDrawable border = new GradientDrawable();
        border.setColor(0xFFFFFFFF);
        border.setStroke(1, getResources().
                getColor(R.color.colorPrimaryDark));

        final boolean[] responseReceived = {false};
        /*
        Dialog customisation
         */
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_save_location);
        final LinearLayout internalStorage = (LinearLayout) dialog.findViewById(R.id.containerInternal);
        final LinearLayout externalStorage = (LinearLayout) dialog.findViewById(R.id.containerExternal);
        final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.fakeLoading);
        ImageView external = (ImageView) dialog.findViewById(R.id.external);

        final SharedPreferences preferences = getSharedPreferences(AppController.packageName, MODE_PRIVATE);
        Boolean isSDPresent = Environment.getExternalStorageState().
                equals(android.os.Environment.MEDIA_MOUNTED);

        Log.d("msg", isSDPresent + "");

        if (!isSDPresent) {
            externalStorage.setEnabled(false);
            external.setImageResource(R.drawable.ic_storage_external_faded_100dp);
        }

        // setOnClickListener()
        internalStorage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                responseReceived[0] = true;
                progressBar.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    internalStorage.setBackgroundDrawable(border);
                    externalStorage.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                } else {
                    internalStorage.setBackground(border);
                    externalStorage.setBackground(new ColorDrawable(Color.TRANSPARENT));
                }
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.SP_STORAGE_TAG, Constants.SP_STORAGE_VAL_INTERNAL);
                editor.apply();
                internalStorage.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 500);
            }
        });

        externalStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                responseReceived[0] = true;
                progressBar.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    externalStorage.setBackgroundDrawable(border);
                    internalStorage.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                } else {
                    externalStorage.setBackground(border);
                    internalStorage.setBackground(new ColorDrawable(Color.TRANSPARENT));
                }
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.SP_STORAGE_TAG, Constants.SP_STORAGE_VAL_EXTERNAL);
                editor.apply();
                internalStorage.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 500);
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                if (!responseReceived[0]) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.SP_STORAGE_TAG, Constants.SP_STORAGE_VAL_INTERNAL);
                    editor.apply();

                    Snackbar.make(mViewPager, "Internal Storage has been set as default." +
                            "\nThis preference in downloads Section", Snackbar.LENGTH_LONG).show();
                }
            }
        });
        dialog.show();
    }

    private void initialize() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mViewPager = (ViewPager) findViewById(R.id.container);
        assert mViewPager != null;
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        assert tabLayout != null;
        tabLayout.setupWithViewPager(mViewPager);
        if (tabLayout.getTabCount() >= 2) {
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_movie_white_24dp);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_tv_large);
        }

        updateContainer = (FrameLayout) findViewById(R.id.updaterContainer);
        updateStatus = (TextView) findViewById(R.id.statusUpdates);
        databaseUpdateProgress = (ProgressBar) findViewById(R.id.progress);

        /*
        Ask for download location preferences after some time
         */
//        final SharedPreferences preferences = getSharedPreferences(
//                AppController.packageName, Context.MODE_PRIVATE);
//        boolean firstAction = preferences.getBoolean("first instance", true);
//        if (firstAction) {
//            mViewPager.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    showEditLocationDialog();
//                    SharedPreferences.Editor editor = preferences.edit();
//                    editor.putBoolean("first instance", false);
//                    editor.apply();
//                }
//            }, 3000);
//        }
    }

    public void setUpdateMessage(String msg) {
        updateStatus.setText(msg);
    }

    @Override
    public void startActivity(Intent intent) {

        Log.d("msg", "in start callingClass");
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            Log.d("msg", "inside " + mViewPager.getCurrentItem());
            Bundle appData = new Bundle();
            appData.putInt(SearchableActivity.FRAGMENT, mViewPager.getCurrentItem());
            intent.putExtra(SearchManager.APP_DATA, appData);
        }
        super.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (databaseIsUpdated) {
            switch (id) {
                case R.id.action_settings:
                    return true;

                case R.id.action_refetch:

                    if (!Utils.isConnected(this)) {
                        AlertDialog dialog = new AlertDialog.Builder(this)
                                .setTitle("No Internet Connection!")
                                .setMessage("You are not connected to Internet and this action will " +
                                        "delete all local data cache and will attempt to fetch fresh data")
                                .setIcon(R.drawable.ic_internet_black_24dp)
                                .setPositiveButton("I know", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        File dir = getFilesDir();
                                        File file = new File(dir, Constants.MOVIE_DATA_FILE_NAME);
                                        boolean response1 = file.delete();
                                        file = new File(dir, Constants.TV_DATA_FILE_NAME);
                                        boolean response2 = file.delete();

                                        if (response1 && response2) {
                                            Intent goHome = new Intent(MainActivity.this, SplashActivity.class);
                                            startActivity(goHome);
                                        }
                                        MainActivity.this.finish();
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setNegativeButton("Maybe Later", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .create();
                        dialog.show();
                    } else {
                        File dir = getFilesDir();
                        File file = new File(dir, Constants.MOVIE_DATA_FILE_NAME);
                        boolean response1 = file.delete();
                        file = new File(dir, Constants.TV_DATA_FILE_NAME);
                        boolean response2 = file.delete();

                        if (response1 && response2) {
                            Intent goHome = new Intent(MainActivity.this, SplashActivity.class);
                            startActivity(goHome);
                        }
                        MainActivity.this.finish();
                    }

                    return true;

                case R.id.action_share:

                    final ApplicationInfo app = getApplicationContext().getApplicationInfo();
                    ApplicationInfo tmpInfo = null;
                    try {
                        tmpInfo = this.getPackageManager().getApplicationInfo(app.packageName, -1);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    float size = new File(tmpInfo.sourceDir).length() / (1024 * 1024);

                    final AlertDialog ask = new AlertDialog.Builder(this)
                            .setTitle("Want to proceed ?")
                            .setIcon(R.drawable.ic_bluetooth_blue_24dp)
                            .setMessage("You can easily share the application (.apk) file with your friends.\n\nApp Size: " + size + " MB")
                            .setPositiveButton("Sure", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    String filePath = app.sourceDir;
                                    Intent intent = new Intent(Intent.ACTION_SEND);
                                    intent.setType("*/*");
                                    intent.setPackage("com.android.bluetooth");
                                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));

                                    startActivity(Intent.createChooser(intent, "Share app"));
                                }
                            }).setNegativeButton("Nope", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create();

                    ask.show();

                    return true;

                case R.id.action_search:

                    SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete)
                            searchView.findViewById(R.id.search_src_text);
                    if (mViewPager.getCurrentItem() == 0)
                        theTextArea.setHint("Search Movie");
                    else if (mViewPager.getCurrentItem() == 1)
                        theTextArea.setHint("Search Serial");
                    break;

                case R.id.action_download:

                    Intent intent = new Intent(this, DownloadActivityV2.class);
                    startActivity(intent);
                    DownloadActivityV2.appRunning = true;
                    return true;
            }
        } else {
            Toast.makeText(this, "Please wait till Database gets updated!", Toast.LENGTH_LONG).show();
            return false;
        }

        return false;
    }

    @Override
    public ArrayList<FinishedMovie> getMovieData() {
        return globalReadyMovies;
    }

    @Override
    public ArrayList<T_FetchContentVahid.Serial> getSerialData() {
        return globalReadySerials;
    }

    public static class rawMovie {

        public String name = "";
        public String yr, url, size;

        public rawMovie() {

        }

        public rawMovie(String findInName, String url, String sz) {

            this.url = url;
            String _dotParts[] = findInName.trim().split("\\.");
            int pos = -1;
            for (String part : _dotParts) {
                pos++;
                if (part.contains("0p")) {
                    break;
                }
            }
            for (int i = 0; i < pos - 1; i++) {
                this.name += " " + _dotParts[i];
            }
            yr = _dotParts[pos - 1];
            size = sz;
        }

        public rawMovie(String findInName, String url, String sz, String yr) {

            this.url = url;
            String _dotParts[] = findInName.trim().split("\\.");
            int pos = -1;
            for (String part : _dotParts) {
                pos++;
                if (part.contains("0p") || part.contains("0P")) {
                    break;
                }
            }
            for (int i = 0; i < pos - 1; i++) {
                if (i != 0)
                    name += " " + _dotParts[i];
                else
                    name += _dotParts[i];
            }

            int position = -1;
            if ((position = name.indexOf("(fars")) != -1) {
                name = name.substring(0, position);
            }
            if ((position = name.indexOf("Fars")) != -1) {
                name = name.substring(0, position);
            }
            if ((position = name.indexOf("(")) != -1) {
                name = name.substring(0, position);
            }
            if ((position = name.indexOf("_20")) != -1) {
                name = name.substring(0, position);
            }
            if ((position = name.indexOf("-")) != -1) {
                name = name.substring(0, position);
            }
            name = name.replaceAll("_", " ");
            name = name.replaceAll("HDrip", "");
            name = name.replaceAll("hdrip", "");
            name = name.replaceAll("DVDrip", "");
            name = name.replaceAll("dvdrip", "");
            name = name.replaceAll("DVDRip", "");
            name = name.replaceAll("DVDscr", "");
            name = name.replaceAll("DVDSCR", "");
            name = name.replaceAll("WEBRip", "");
            name = name.replaceAll("Rip", "");
            name = name.replaceAll("rip", "");
            name = name.replaceAll("cam", "");
            name = name.replaceAll("CAM", "");
            name = name.replaceAll("Cam", "");
            name = name.replaceAll("BluRay", "");
            name = name.replaceAll("bluray", "");
            name = name.replaceAll("Bluray", "");
            name = name.replaceAll("BLURAY", "");
            name = name.replaceAll("HDTS", "");
            name = name.replaceAll("HDTV", "");
            name = name.replaceAll("HD", "");
            name = name.replaceAll("TV", "");
            name = name.replaceAll("TS", "");
            name = name.replaceAll("720p", "");
            name = name.replaceAll("720", "");
            name = name.replaceAll("1080p", "");
            name = name.replaceAll("1080", "");
            name = name.replaceAll("IranFilms", "");
            name = name.replaceAll("Ganool", "");

            for (int k = 2000; k < 2020; k++)
                name = name.replaceAll("" + k, "");

            name = name.trim();
//            String p[] = name.split(" ");
//            if (Integer.parseInt(p[p.length - 1]) >= 2000 && Integer.parseInt(p[p.length - 1]) < 3000) {
//
//                for (int i = 0; i < p.length - 1; i++) {
//
//                }
//            }
            this.yr = yr;
            size = sz;
        }
    }

    public static class OMDBResponse_TV {

        public String title, iUrl, rating, description, release, genre, writer, actors, language, runtime;
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new MovieFragment();

                case 1:
                    return new TVSeriesFragment();
            }
            return new BlankFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Movies";
                case 1:
                    return "TV Series";
            }
            return null;
        }
    }

}
