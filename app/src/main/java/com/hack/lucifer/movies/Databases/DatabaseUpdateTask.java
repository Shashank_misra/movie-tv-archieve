package com.hack.lucifer.movies.Databases;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.MainActivity;
import com.hack.lucifer.movies.Serials.DBManager;
import com.hack.lucifer.movies.TVSeriesServers.T_FetchContentVahid;

public class DatabaseUpdateTask extends AsyncTask<Void, Integer, Void> {

    MainActivity callingClass;
    int count = 0;

    public DatabaseUpdateTask(Context context) {
        callingClass = (MainActivity) context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        SharedPreferences preferences = callingClass.getSharedPreferences(
                AppController.packageName,Context.MODE_PRIVATE);
        count = preferences.getInt("count", -1);
        Log.d("msg", "count: " + count);
        callingClass.databaseUpdateProgress.setMax(count);
        callingClass.setUpdateMessage("Updating videos in Database!");
    }

    @Override
    protected Void doInBackground(Void... params) {

        int count = 0;
        for (T_FetchContentVahid.Serial serial : MainActivity.globalReadySerials)
            for (T_FetchContentVahid.Serial.Season season : serial.seasons)
                for (T_FetchContentVahid.Serial.Season.Episode episode : season.episodes) {

                    DBManager manager = new DBManager(callingClass);
                    int response = manager.addEpisode(serial.name, season.seasonNum,
                            episode.getID(), episode.episodeNumber, episode.url);
                    if (response == DBManager.FATAL_ERROR || response == -1)
                        Log.d("msg", response + " for " + episode.getID() + "\nname:" +
                                episode.name + "\nser: " + serial.name + "\nseason: " +
                                season.getSeasonNum());

                    publishProgress(++count);
                }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        callingClass.databaseUpdateProgress.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        callingClass.updateContainer.setVisibility(View.GONE);
        MainActivity.databaseIsUpdated = true;
//        Intent intent = new Intent(callingClass, MainActivity.class);
//        MainActivity.globalReadyMovies = callingClass.movieDataFinal;
//        MainActivity.globalReadySerials = callingClass.serialData;
////            Bundle bundle = new Bundle();
////            bundle.putSerializable("movieDataFinal", callingClass.movieDataFinal);
////            bundle.putSerializable("serialData", callingClass.serialData);
////            intent.putExtras(bundle);
//        callingClass.startActivity(intent);
//        callingClass.finish();
    }
}
