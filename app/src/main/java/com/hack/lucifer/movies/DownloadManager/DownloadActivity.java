package com.hack.lucifer.movies.DownloadManager;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hack.lucifer.movies.AppController;
import com.hack.lucifer.movies.Constants;
import com.hack.lucifer.movies.Databases.Download_DB_helper;
import com.hack.lucifer.movies.R;
import com.hack.lucifer.movies.SplashActivity;

import java.io.File;
import java.util.ArrayList;

public class DownloadActivity extends AppCompatActivity {

    public static boolean appRunning = false;
    private DBManager downDBManager;
    private com.hack.lucifer.movies.Serials.DBManager serialDBManager;
    private ArrayList<dataPacket> listData = new ArrayList<>();
    private DownloadManager downloadManager;

    private boolean isShowing = false;

    private RecyclerView downloadList;
    private RelativeLayout infoContainer;
    private TextView savePath;
    private ImageView editPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        if (getActionBar() != null) {
            getActionBar().setHomeButtonEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        }

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        downDBManager = new DBManager(this);
        serialDBManager = new com.hack.lucifer.movies.Serials.DBManager(this);
        obtainData();
        assignReferences();
        savePath.setText(Environment.DIRECTORY_DOWNLOADS
                + File.separator + Constants.DIRECTORY_NAME);
    }

    private void showEditLocationDialog() {

        /*
        Code for creating border of selected item
         */
        final GradientDrawable border = new GradientDrawable();
        border.setColor(0xFFFFFFFF);
        border.setStroke(1, getResources().
                getColor(R.color.colorPrimaryDark));

        /*
        Dialog customisation
         */
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_save_location);
        final LinearLayout internalStorage = (LinearLayout) dialog.findViewById(R.id.containerInternal);
        final LinearLayout externalStorage = (LinearLayout) dialog.findViewById(R.id.containerExternal);
        final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.fakeLoading);
        ImageView external = (ImageView) dialog.findViewById(R.id.external);


        // Check If external card is present
        final SharedPreferences preferences = getSharedPreferences(AppController.packageName, MODE_PRIVATE);

        final Boolean isSDPresent = Environment.getExternalStorageState().
                equals(android.os.Environment.MEDIA_MOUNTED);

        File file = new File("/mnt/extSdCard");
        try {
            File list[] = file.listFiles();
            Toast.makeText(getApplicationContext(), "Yes sdcard is mounted, file count " + list.length, Toast.LENGTH_LONG).show();
        } catch (NullPointerException o) {
            Toast.makeText(getApplicationContext(), "Sorry no sdcard is mounted:", Toast.LENGTH_LONG).show();
        }

        Log.d("msg", "check results " + isSDPresent + " with " + Environment.getExternalStorageState());

        if (!isSDPresent) {
            Log.d("msg", "no card found");
            externalStorage.setEnabled(false);
            external.setImageResource(R.drawable.ic_storage_external_faded_100dp);
        }

        // Mark the default selection
        LinearLayout selectedTarget = null;
        String location = preferences.getString(Constants.SP_STORAGE_TAG, "");
        if (location.equalsIgnoreCase(Constants.SP_STORAGE_VAL_INTERNAL))
            selectedTarget = internalStorage;
        else if (location.equalsIgnoreCase(Constants.SP_STORAGE_VAL_EXTERNAL))
            selectedTarget = externalStorage;

        if (selectedTarget != null)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                selectedTarget.setBackgroundDrawable(border);
            } else {
                selectedTarget.setBackground(border);
            }

        // setOnClickListener()
        internalStorage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    internalStorage.setBackgroundDrawable(border);
                    externalStorage.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                } else {
                    internalStorage.setBackground(border);
                    externalStorage.setBackground(new ColorDrawable(Color.TRANSPARENT));
                }
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.SP_STORAGE_TAG, Constants.SP_STORAGE_VAL_INTERNAL);
                editor.apply();
                internalStorage.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 500);
            }
        });

        externalStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isSDPresent) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        externalStorage.setBackgroundDrawable(border);
                        internalStorage.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    } else {
                        externalStorage.setBackground(border);
                        internalStorage.setBackground(new ColorDrawable(Color.TRANSPARENT));
                    }
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Constants.SP_STORAGE_TAG, Constants.SP_STORAGE_VAL_EXTERNAL);
                    editor.apply();
                    internalStorage.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 500);
                }
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {

        if (!appRunning) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_downlaod, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_info:
                if (isShowing) {
                    infoContainer.animate().translationY(-1 * infoContainer.getHeight()).setDuration(300);
                    downloadList.animate().translationY(-1 * infoContainer.getHeight()).setDuration(300);
                    isShowing = false;
                } else {
                    infoContainer.animate().translationY(0).setDuration(300);
                    downloadList.animate().translationY(0).setDuration(300);
                    isShowing = true;
                }
        }
        return true;
    }

    private void obtainData() {

        Cursor cursor = downDBManager.getAllEntries();
        listData.clear();
        if (cursor.moveToFirst())
            do {
                try {
                    String name = cursor.getString(cursor.getColumnIndex(Download_DB_helper.NAME));
                    Download_DB_helper.STATES state = Download_DB_helper.STATES.getState(
                            cursor.getInt(
                                    cursor.getColumnIndex(
                                            Download_DB_helper.STATE)));
                    long id = cursor.getLong(cursor.getColumnIndex(Download_DB_helper.DOWNLOAD_ID));
                    String videoId = cursor.getString(cursor.getColumnIndex(Download_DB_helper.VIDEO_ID));
                    String url = cursor.getString(cursor.getColumnIndex(Download_DB_helper.URL));
                    String relativePath = cursor.getString(cursor.getColumnIndex(Download_DB_helper.SAVE_PATH));
                    if (state != Download_DB_helper.STATES.FAILED &&
                            state != Download_DB_helper.STATES.COMPLETE)

                        listData.add(new dataPacket(name, id, state, videoId, url, relativePath));
                } catch (Exception ignored) {
                    Log.e("msg", ignored.getMessage());
                }
            } while (cursor.moveToNext());

        cursor.close();
    }

    private Download_DB_helper.STATES getStateFor(int state_db) {

        switch (state_db) {
            case DownloadManager.STATUS_FAILED:
                return Download_DB_helper.STATES.FAILED;
            case DownloadManager.STATUS_PAUSED:
                return Download_DB_helper.STATES.PAUSED;
            case DownloadManager.STATUS_PENDING:
                return Download_DB_helper.STATES.PENDING;
            case DownloadManager.STATUS_RUNNING:
                return Download_DB_helper.STATES.ONGOING;
            case DownloadManager.STATUS_SUCCESSFUL:
                return Download_DB_helper.STATES.COMPLETE;
        }
        return Download_DB_helper.STATES.UNAVAILABLE;
    }

    private void assignReferences() {

        infoContainer = (RelativeLayout) findViewById(R.id.c1);
        downloadList = (RecyclerView) findViewById(R.id.downloadList);
//        downloadList.setAdapter(new adapter(listData, this));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        downloadList.setLayoutManager(mLayoutManager);
        downloadList.setItemAnimator(new DefaultItemAnimator());
        savePath = (TextView) findViewById(R.id.savePath);
        editPath = (ImageView) findViewById(R.id.editLocation);

//        editPath.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showEditLocationDialog();
//            }
//        });
    }

    public class dataPacket {
        public String name;
        public long downloadId;
        public String videoId;
        public String url;
        public String relativePath;
        public Download_DB_helper.STATES state;

        public dataPacket(String name, long id, Download_DB_helper.STATES state, String videoId,
                          String url, String relativePath) {
            this.name = name;
            this.downloadId = id;
            this.state = state;
            this.videoId = videoId;
            this.url = url;
            this.relativePath = relativePath;
        }
    }

//    class adapter extends RecyclerView.Adapter<adapter.Holder> {
//
//        ArrayList<dataPacket> data;
//        DownloadActivity activity;
//
//        adapter(ArrayList<dataPacket> data, DownloadActivity activity) {
//            this.data = data;
//            this.activity = activity;
//        }
//
//        @Override
//        public adapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
//            View itemView = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.row_doanload_list, parent, false);
//            return new Holder(itemView);
//        }
//
//        @Override
//        public void onBindViewHolder(final adapter.Holder holder, int position) {
//
//            final SharedPreferences setting = activity.getSharedPreferences(Constants.DOWNLOAD_ID_PREFERENCE_FILE,
//                    Context.MODE_APPEND);
//            final dataPacket dataP = data.get(position);
//            holder.title.setText(dataP.name);
//            Log.d("states", dataP.state.toString());
//
//            if (dataP.state == Download_DB_helper.STATES.ONGOING ||
//                    dataP.state == Download_DB_helper.STATES.PENDING ||
//                    dataP.state == Download_DB_helper.STATES.PAUSED) {
//
//                holder.restart.setEnabled(false);
//                holder.restart.setImageResource(R.drawable.ic_restart_green_disabled_24dp);
//                holder.stop.setEnabled(true);
//                holder.stop.setImageResource(R.drawable.ic_clear_green_enabled_24dp);
//                new Thread(new Runnable() {
//                    {
//
//                    }
//
//                    @Override
//                    public void run() {
//
//                        boolean downloading = true;
//                        final boolean[] updatedSize = {false};
//
//                        while (downloading) {
//
//                            DownloadManager.Query q = new DownloadManager.Query();
//                            q.setFilterById(dataP.downloadId);
//
//                            Cursor cursor = downloadManager.query(q);
//                            if (cursor.moveToFirst()) {
//                                float bytes_downloaded = cursor.getInt(cursor
//                                        .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
//                                float bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
//
//                                final int dl_progress = (int) ((bytes_downloaded * 100L) / bytes_total);
//
//                                String unit_t = "B";
//                                if (bytes_total > 1024 + 100) {
//
//                                    bytes_total = bytes_total / 1024;
//                                    unit_t = "KB";
//                                    if (bytes_total > 1024 + 100) {
//
//                                        bytes_total = bytes_total / 1024;
//                                        unit_t = "MB";
//                                        if (bytes_total > 1024 + 100) {
//
//                                            bytes_total = bytes_total / 1024;
//                                            unit_t = "GB";
//                                        }
//                                    }
//                                }
//
//                                String unit_d = "B";
//                                if (bytes_downloaded > 1024 + 100) {
//
//                                    bytes_downloaded = bytes_downloaded / 1024;
//                                    unit_d = "KB";
//                                    if (bytes_downloaded > 1024 + 100) {
//
//                                        bytes_downloaded = bytes_downloaded / 1024;
//                                        unit_d = "MB";
//                                        if (bytes_downloaded > 1024 + 100) {
//
//                                            bytes_downloaded = bytes_downloaded / 1024;
//                                            unit_d = "GB";
//                                        }
//                                    }
//                                }
//
//                                final String total = new DecimalFormat("#.##").format(bytes_total) + unit_t;
//                                final String complete = new DecimalFormat("#.##").format(bytes_downloaded) + unit_d;
//
//                                /*
//                                update total size at-least once when download ongoing
//                                 */
//                                if (!updatedSize[0]) {
//                                    activity.runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            String size = downDBManager.getVideoSizeForDownloadID(dataP.downloadId);
//                                            if (size.equalsIgnoreCase("-1B")) {
//                                                downDBManager.updateSizeForDownloadId(size, dataP.downloadId);
//                                                updatedSize[0] = true;
//                                            }
//                                        }
//                                    });
//                                }
//
//                                /*
//                                update visible list on download complete
//                                 */
//                                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
//                                    downloading = false;
//                                    dataP.state = Download_DB_helper.STATES.COMPLETE;
//                                    holder.stop.setEnabled(false);
//                                    activity.runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            notifyDataSetChanged();
//                                        }
//                                    });
//                                }
//
//                                DownloadActivity.this.runOnUiThread(new Runnable() {
//
//                                    @Override
//                                    public void run() {
//                                        holder.progressBar.setProgress(dl_progress);
//                                        holder.downloaded.setText(complete);
//                                        holder.total.setText(total);
//                                    }
//                                });
//
//                                cursor.close();
//                            } else {
//                                downloading = false;
//                                Log.d("msg", "updating reference " + dataP.downloadId + " with failed");
//                                activity.runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        downDBManager.updateStateForDownloadId(Download_DB_helper.STATES.FAILED, dataP.downloadId);
//                                    }
//                                });
//                                dataP.state = Download_DB_helper.STATES.FAILED;
//                            }
//                        }
//                    }
//                }).start();
//            } else if (dataP.state == Download_DB_helper.STATES.COMPLETE) {
//
//                holder.outcome.setVisibility(View.VISIBLE);
//                holder.outcome.setText("Complete");
//                holder.outcome.setTextColor(Color.GREEN);
//                holder.progressBar.setProgress(holder.progressBar.getMax());
//                holder.downloaded.setVisibility(View.GONE);
//
//                holder.total.setText(downDBManager.getVideoSizeForDownloadID(dataP.downloadId));
//                holder.stop.setEnabled(false);
//                holder.stop.setImageResource(R.drawable.ic_clear_green_disabled_24dp);
//                holder.restart.setEnabled(false);
//                holder.restart.setImageResource(R.drawable.ic_restart_green_disabled_24dp);
//
//            } else if (dataP.state == Download_DB_helper.STATES.FAILED ||
//                    dataP.state == Download_DB_helper.STATES.UNAVAILABLE) {
//
//                holder.outcome.setVisibility(View.VISIBLE);
//                holder.outcome.setText("Failed");
//                holder.outcome.setTextColor(Color.RED);
//                holder.progressBar.setProgress(0);
//                holder.total.setText(downDBManager.getVideoSizeForDownloadID(dataP.downloadId));
//                holder.stop.setEnabled(false);
//                holder.stop.setImageResource(R.drawable.ic_clear_green_disabled_24dp);
//                holder.restart.setEnabled(true);
//                holder.restart.setImageResource(R.drawable.ic_restart_green_enabled_24dp);
//            }
//
//            holder.restart.setOnClickListener(new View.OnClickListener() {
//                {
//
//                }
//
//                @Override
//                public void onClick(View view) {
//
//                    AppController.feedback();
//                    final long downloadId = addVideoToDownloadManager(dataP.downloadId, dataP.name);
//                    if (downloadId != -1) {
//                        holder.stop.setEnabled(true);
//                        holder.stop.setImageResource(R.drawable.ic_clear_green_enabled_24dp);
//                        holder.restart.setEnabled(false);
//                        holder.restart.setImageResource(R.drawable.ic_restart_green_disabled_24dp);
//                        holder.outcome.setVisibility(View.GONE);
//
//                        /*
//                        Determining state of download just added: ONGOING or PENDING
//                         */
//                        DownloadManager.Query q = new DownloadManager.Query();
//                        q.setFilterById(downloadId);
//                        Cursor cursor = downloadManager.query(q);
//                        cursor.moveToFirst();
//                        int state_db = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
//                        Download_DB_helper.STATES state = getStateFor(state_db);
//
//                        /*
//                        update shared preference state
//                         */
//                        final SharedPreferences.Editor edit = setting.edit();
//                        edit.remove(downloadId + "");
//                        edit.putLong(downloadId + "", state_db);
//                        edit.commit();
//
//                        /*
//                        update downloads database
//                         */
//                        int result = downDBManager.updateRestartedDownloadId(downloadId, dataP.videoId, state);
//                        dataP.downloadId = downloadId;
//                        dataP.state = state;
//                        notifyDataSetChanged();
//                        Log.d("msg", "restarted id: " + result + "");
//                    } else {
//                        Toast.makeText(activity,
//                                "Something went wrong!\n Try restarting download from details page",
//                                Toast.LENGTH_LONG).show();
//                    }
//                }
//            });
//            holder.stop.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    AppController.feedback();
//                    downloadManager.remove(dataP.downloadId);
//                    downDBManager.updateStateForDownloadId(Download_DB_helper.STATES.FAILED, dataP.downloadId);
//                    holder.restart.setEnabled(true);
//                    holder.restart.setImageResource(R.drawable.ic_restart_green_enabled_24dp);
//                    holder.downloaded.setText("0B");
//                    holder.progressBar.setProgress(0);
//                    dataP.state = Download_DB_helper.STATES.FAILED;
//                    notifyDataSetChanged();
//                    holder.stop.setEnabled(false);
//                    holder.stop.setImageResource(R.drawable.ic_clear_green_disabled_24dp);
//                }
//            });
//        }
//
//        @Override
//        public int getItemCount() {
//            return data.size();
//        }
//
//        private Download_DB_helper.STATES getStateFor(int state_db) {
//
//            switch (state_db) {
//                case DownloadManager.STATUS_FAILED:
//                    return Download_DB_helper.STATES.FAILED;
//                case DownloadManager.STATUS_PAUSED:
//                    return Download_DB_helper.STATES.PAUSED;
//                case DownloadManager.STATUS_PENDING:
//                    return Download_DB_helper.STATES.PENDING;
//                case DownloadManager.STATUS_RUNNING:
//                    return Download_DB_helper.STATES.ONGOING;
//            }
//            return null;
//        }
//
//        private long addVideoToDownloadManager(long downId, String name) {
//
//            String videoId = downDBManager.getVideoIdForDownloadId(downId);
//            Log.d("msg", "querying url for video id: " + videoId);
//            String url = serialDBManager.getVideoURL(videoId);
//            if (url.length() > 0) {
//                Uri Download_Uri = Uri.parse(url);
//                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
//                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
//                        | DownloadManager.Request.NETWORK_MOBILE);
//                request.setTitle(name);
//                request.setDescription("Movie TV");
//                request.allowScanningByMediaScanner();
//
//                String filePath = downDBManager.getFilePathForDownId(downId);
//                if (filePath == null) {
//                    Log.d("msg", "path not found in DB");
//                    return -1;
//                }
//
////                request.setDestinationInExternalFilesDir(activity, Environment.DIRECTORY_DOWNLOADS, name);
//                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
//                        filePath);
//                return downloadManager.enqueue(request);
//            } else {
//                Log.d("msg", "url length was zero");
//                return -1;
//            }
//        }
//
//        public class Holder extends RecyclerView.ViewHolder {
//
//            public TextView title, downloaded, total, outcome;
//            public ProgressBar progressBar;
//            public ImageButton stop, restart;
//            public boolean restartedDownload = false;
//
//            public Holder(View itemView) {
//                super(itemView);
//                title = (TextView) itemView.findViewById(R.id.identificationTitle);
//                downloaded = (TextView) itemView.findViewById(R.id.downloadedSize);
//                total = (TextView) itemView.findViewById(R.id.TotalSize);
//                progressBar = (ProgressBar) itemView.findViewById(R.id.progress);
//                outcome = (TextView) itemView.findViewById(R.id.outCome);
//                stop = (ImageButton) itemView.findViewById(R.id.stop);
//                restart = (ImageButton) itemView.findViewById(R.id.restart);
//            }
//        }
//
//    }
}
