//package com.hack.lucifer.movies.TVSeriesServers;
//
//import android.app.Activity;
//import android.os.AsyncTask;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.hack.lucifer.movies.MainActivity;
//
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//
//import java.io.IOException;
//import java.util.ArrayList;
//
//
//public class T_FetchContent98Music extends AsyncTask<Void, String, String> {
//
//
//    MainActivity callingClass;
//    String serverUrl;
//
//    public T_FetchContent98Music(Activity activity, String serverUrl) {
//
//        callingClass = (MainActivity) activity;
//        this.serverUrl = serverUrl;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        callingClass.loading.setMessage("Connecting to TV Server 1");
//        if (!callingClass.loading.isShowing()) {
//            callingClass.loading.show();
//        }
//    }
//
//    @Override
//    protected String doInBackground(Void... voids) {
//
//        /*
//        Fetch series list
//         */
//        Document document = null;
//        ArrayList<T_FetchContentVahid.Serial> serials = new ArrayList<>();
//        try {
//            document = Jsoup.connect(serverUrl).timeout(60000).get();
//            Elements links = document.select("a[href]");
//
//            for (Element link : links) {
//
//                String name = link.text();
//                String url = link.attr("abs:href");
//                if (!name.equalsIgnoreCase("Parent Directory")) {
//                    name = name.trim();
//                    serials.add(new T_FetchContentVahid.Serial(name, url));
//                }
//                publishProgress("Found " + serials.size() + " links");
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            publishProgress("Crashed while Connecting to base");
//            return null;
//        }
//
//        /*
//        Fetch seasons for each series
//         */
//        for (T_FetchContentVahid.Serial serial : serials) {
//            try {
//                document = Jsoup.connect(serial.url).timeout(60000).get();
//                publishProgress("Finding Seasons for\n" + serial.name);
//                Elements links = document.select("a[href]");
//                for (Element link : links) {
//                    String name = link.text();
//                    String url = link.attr("abs:href");
//                    if (!name.equalsIgnoreCase("Parent Directory") && !name.equalsIgnoreCase("sub")) {
//                        name = name.trim();
//                        serial.seasons.add(new T_FetchContentVahid.Serial.Season(name, url));
//                    }
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        /*
//        Fetch the episodes for all seasons of all series << HEAVIEST TASK !!!>>
//         */
//        for (T_FetchContentVahid.Serial serial : serials) {
//            for (T_FetchContentVahid.Serial.Season season : serial.seasons) {
//
//                try {
//                    document = Jsoup.connect(season.getUrl()).timeout(60000).get();
//                    Elements links = document.select("a[href]");
//                    for (Element link : links) {
//                        String name = link.text();
//                        String url = link.attr("abs:href");
//                        if (!name.equalsIgnoreCase("Parent Directory") && !name.endsWith(".zip") && !name.equalsIgnoreCase("sub")) {
//                            name = name.trim();
//                            boolean res = season.episodes.add(new T_FetchContentVahid.Serial.Season.Episode(name, url));
//                            publishProgress("" + season.getName() + ": " + name);
////                            Log.d("msg", res + " name: " + name + " url: " + url);
//                        }
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        for (T_FetchContentVahid.Serial s : serials)
//            Log.d("msg", s.toString());
//        return null;
//    }
//
//    @Override
//    protected void onProgressUpdate(String... values) {
//        super.onProgressUpdate(values);
//
//        if (values[0].equals("_")) {
//            callingClass.loading.hide();
//            Toast.makeText(callingClass, "CRASHED", Toast.LENGTH_SHORT).show();
//        } else {
//            callingClass.loading.setMessage(values[0]);
//        }
//    }
//
//    @Override
//    protected void onPostExecute(String s) {
//        super.onPostExecute(s);
//
////        T_FetchContentVahid check2 = new T_FetchContentVahid(callingClass, Constants.BASE_URL_UPLOADPLUS);
////        check2.execute();
//    }
//}
