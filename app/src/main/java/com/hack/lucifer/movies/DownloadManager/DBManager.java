package com.hack.lucifer.movies.DownloadManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hack.lucifer.movies.Databases.Download_DB_helper;

import java.util.HashSet;


public class DBManager {

    Context activityContext;
    Download_DB_helper worker;
    public static final int ALREADY_PRESENT = 100;
    public static final int VIDEO_NOT_PRESENT = 101;

    public DBManager(Context context) {
        activityContext = context;
        worker = new Download_DB_helper(activityContext);
    }

    public int addDownload(long downloadId, String videoId, Download_DB_helper.STATES state, String name, String size, String url,
                           String save_path) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(" + Download_DB_helper.VIDEO_ID + ") from " +
                Download_DB_helper.TABLE + " where " + Download_DB_helper.VIDEO_ID + " = " + videoId, null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        Log.d("msg", "id: " + videoId + " has count " + count);
        db.close();
        db.releaseReference();

        if (count > 0) {
            Download_DB_helper.STATES savedState = getVideoStateForVideoID(videoId);
            ContentValues values = new ContentValues();
            values.put(Download_DB_helper.STATE, state.getVal());
            values.put(Download_DB_helper.DOWNLOAD_ID, downloadId);
            values.put(Download_DB_helper.URL, url);
            values.put(Download_DB_helper.SIZE, size);
            values.put(Download_DB_helper.NAME, name);
            values.put(Download_DB_helper.SAVE_PATH, save_path);

            db = worker.getWritableDatabase();
            int updates = db.update(Download_DB_helper.TABLE, values, Download_DB_helper.VIDEO_ID + " = ? ", new String[]{videoId});
            db.close();
            db.releaseReference();
            Log.d("msg", "DownTable entry found; updated rows: " + updates);
            return updates;
        }

        if (count == 0) {

            db = worker.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Download_DB_helper.DOWNLOAD_ID, downloadId);
            values.put(Download_DB_helper.STATE, state.getVal());
            values.put(Download_DB_helper.NAME, name);
            values.put(Download_DB_helper.VIDEO_ID, videoId);
            values.put(Download_DB_helper.SIZE, size);
            values.put(Download_DB_helper.URL, url);
            values.put(Download_DB_helper.SAVE_PATH, save_path);

            long result = db.insert(Download_DB_helper.TABLE, null, values);
            db.close();
            db.releaseReference();
            Log.d("msg", "added video: " + result);
            return (int) result;
        } else return ALREADY_PRESENT;
    }

    public int updateRestartedDownloadId(long newDownloadId, String videoId, Download_DB_helper.STATES state) {

        SQLiteDatabase db = worker.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(Download_DB_helper.DOWNLOAD_ID, newDownloadId);
        value.put(Download_DB_helper.STATE, state.getVal());
        int result = db.update(Download_DB_helper.TABLE, value, Download_DB_helper.VIDEO_ID + " =?", new String[]{videoId});
        db.close();
        db.releaseReference();
        return result;
    }

    public int getVideoStateForDownloadID(long id) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.query(Download_DB_helper.TABLE, new String[]{Download_DB_helper.STATE},
                Download_DB_helper.DOWNLOAD_ID +
                        " = ?", new String[]{id + ""},
                null, null, null);
        cursor.moveToFirst();
        int val = cursor.getInt(0);
        cursor.close();
        db.close();
        db.releaseReference();
        return val;
    }

    public Cursor getAllEntries() {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.query(Download_DB_helper.TABLE, new String[]{
                        Download_DB_helper.STATE,
                        Download_DB_helper.VIDEO_ID,
                        Download_DB_helper.NAME,
                        Download_DB_helper.DOWNLOAD_ID,
                        Download_DB_helper.URL,
                        Download_DB_helper.SAVE_PATH},
                null, null,
                null, null, null);
        return cursor;
    }

    public Cursor getAllAbout(long id) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.query(Download_DB_helper.TABLE, new String[]{
                        Download_DB_helper.STATE,
                        Download_DB_helper.NAME},
                Download_DB_helper.VIDEO_ID +
                        " = ?", new String[]{id + ""},
                null, null, null);
        db.close();
        db.releaseReference();
        return cursor;
    }

    public int updateStateForDownloadId(Download_DB_helper.STATES state, long downloadId) {

        SQLiteDatabase db = worker.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Download_DB_helper.STATE, state.getVal());
        values.put(Download_DB_helper.SAVE_PATH, "");
        int row = db.update(Download_DB_helper.TABLE, values,
                Download_DB_helper.DOWNLOAD_ID + " =?", new String[]{downloadId + ""});

        db.close();
        db.releaseReference();
        return row;
    }

    public String getVideoIdForDownloadId(long downloadId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Download_DB_helper.VIDEO_ID +
                " from " + Download_DB_helper.TABLE + " where " +
                Download_DB_helper.DOWNLOAD_ID + " = ?", new String[]{downloadId + ""});
        String result;
        if (cursor.moveToFirst())
            result = cursor.getString(cursor.getColumnIndex(Download_DB_helper.VIDEO_ID));
        else
            result = null;
        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }

    public String getVideoSizeForDownloadID(long downloadId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Download_DB_helper.SIZE +
                " from " + Download_DB_helper.TABLE + " where " +
                Download_DB_helper.DOWNLOAD_ID + " = ?", new String[]{downloadId + ""});
        String result;
        if (cursor.moveToFirst())
            result = cursor.getString(cursor.getColumnIndex(Download_DB_helper.SIZE));
        else
            result = null;
        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }

    public Download_DB_helper.STATES getVideoStateForVideoID(String videoId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.query(Download_DB_helper.TABLE, new String[]{Download_DB_helper.STATE},
                Download_DB_helper.VIDEO_ID +
                        " = ?", new String[]{videoId + ""},
                null, null, null);
        int val = -1;
        if (cursor.moveToFirst()) {
            val = cursor.getInt(cursor.getColumnIndex(Download_DB_helper.STATE));
        }
        cursor.close();
        db.close();
        db.releaseReference();

        return Download_DB_helper.STATES.getState(val);
    }

    public long getDownloadIdForVideoId(String videoId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.query(Download_DB_helper.TABLE, new String[]{Download_DB_helper.DOWNLOAD_ID},
                Download_DB_helper.VIDEO_ID +
                        " = ?", new String[]{videoId + ""},
                null, null, null);
        cursor.moveToFirst();
        long val = cursor.getLong(0);
        cursor.close();
        db.close();
        db.releaseReference();
        return val;
    }

    public HashSet<Long> getAllDownloadIds() {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT distinct " + Download_DB_helper.DOWNLOAD_ID + " from "
                + Download_DB_helper.TABLE, null);

        HashSet<Long> result = new HashSet<>();
        if (cursor.moveToFirst()) {

            do {
                result.add(cursor.getLong(cursor.getColumnIndex(Download_DB_helper.DOWNLOAD_ID)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }

    public boolean removeDownloadForDownloadId(long downloadId) {

        SQLiteDatabase db = worker.getWritableDatabase();
        boolean result = db.delete(Download_DB_helper.TABLE, Download_DB_helper.DOWNLOAD_ID + "= ? ", new String[]{downloadId + ""}) > 0;
        db.close();
        db.releaseReference();
        return result;
    }

    public boolean removeDownloadForVideoID(String videoID) {

        SQLiteDatabase db = worker.getWritableDatabase();
        boolean result = db.delete(Download_DB_helper.TABLE, Download_DB_helper.VIDEO_ID + "= ? ", new String[]{videoID}) > 0;
        db.close();
        db.releaseReference();
        return result;
    }

    public String getVideoURLForDownloadId(long downloadId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Download_DB_helper.URL +
                " from " + Download_DB_helper.TABLE + " where " +
                Download_DB_helper.DOWNLOAD_ID + " = ?", new String[]{downloadId + ""});
        String result;
        if (cursor.moveToFirst())
            result = cursor.getString(cursor.getColumnIndex(Download_DB_helper.URL));
        else
            result = null;
        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }

    public String getVideoNameForD_ID(long downId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Download_DB_helper.NAME +
                " from " + Download_DB_helper.TABLE + " where " +
                Download_DB_helper.DOWNLOAD_ID + " = ?", new String[]{downId + ""});
        String result;
        if (cursor.moveToFirst())
            result = cursor.getString(cursor.getColumnIndex(Download_DB_helper.NAME));
        else
            result = null;
        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }

    public boolean updateSizeForDownloadId(String size, long downloadId) {

        SQLiteDatabase db = worker.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Download_DB_helper.SIZE, size);
        int row = db.update(Download_DB_helper.TABLE, values,
                Download_DB_helper.DOWNLOAD_ID + " =?", new String[]{downloadId + ""});

        db.close();
        db.releaseReference();
        return row > 0;
    }

    public String getFilePathForDownId(long downId) {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Download_DB_helper.SAVE_PATH +
                " from " + Download_DB_helper.TABLE + " where " +
                Download_DB_helper.DOWNLOAD_ID + " = ?", new String[]{downId + ""});
        String result;
        if (cursor.moveToFirst())
            result = cursor.getString(cursor.getColumnIndex(Download_DB_helper.SAVE_PATH));
        else
            result = null;
        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }

    public String[][] getStoragePathsWithVideoID() {

        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + Download_DB_helper.SAVE_PATH +
                ", " + Download_DB_helper.VIDEO_ID +
                " from " + Download_DB_helper.TABLE, new String[]{});
        String[][] result = new String[cursor.getCount()][2];
        cursor.moveToFirst();
        int i = 0;
        do {
            result[i][0] = cursor.getString(cursor.getColumnIndex(Download_DB_helper.SAVE_PATH));
            result[i++][1] = cursor.getString(cursor.getColumnIndex(Download_DB_helper.VIDEO_ID));
        } while (cursor.moveToNext());

        cursor.close();
        db.close();
        db.releaseReference();
        return result;
    }
}
