package com.hack.lucifer.movies.Movies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.devbrackets.android.exomedia.EMVideoView;
import com.devbrackets.android.exomedia.listener.ExoPlayerListener;
import com.hack.lucifer.movies.FontUtils;
import com.hack.lucifer.movies.R;
import com.hack.lucifer.movies.Utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by varnit_khandelwal on 06/08/16.
 *
 * @company SilverPush
 */
public class MoviePlayActivity extends Activity {
    private static EMVideoView mVideoView;
    private static SeekBar seekBar;
    private static ProgressBar progressBar;
    private static TextView videoCurrentTime;
    private TextView videoDuration;
    private static ImageView play;
    private ImageView fullscreen;
    private FinishedMovie finishedMovie;
    private TextView title, actors, plot, releaseDate, rating, awards, genre, runtime, director, awardsText, directorText, starring, releaseDateText;
    private RatingBar ratingStars;
    private boolean isFullScreenClicked;
    private LinearLayout timer;
    private boolean everyThingHidden = false;
    private android.os.Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_movie_play);
        intialize();
    }

    private void intialize() {
        mVideoView = (EMVideoView) findViewById(R.id.VideoView);
        seekBar = (SeekBar) findViewById(R.id.seek1);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        videoCurrentTime = (TextView) findViewById(R.id.videoCurrentTime);
        videoDuration = (TextView) findViewById(R.id.videoDuration);
//        titleVideo = (TextView) findViewById(R.id.titleVideo);
        play = (ImageView) findViewById(R.id.play);
        fullscreen = (ImageView) findViewById(R.id.fullscreen);

        title = (TextView) findViewById(R.id.title);
        actors = (TextView) findViewById(R.id.actors);
        plot = (TextView) findViewById(R.id.plot);
        releaseDate = (TextView) findViewById(R.id.releaseDate);
        rating = (TextView) findViewById(R.id.rating);
        ratingStars = (RatingBar) findViewById(R.id.ratingBar);
        awards = (TextView) findViewById(R.id.awards);
        genre = (TextView) findViewById(R.id.genre);
        runtime = (TextView) findViewById(R.id.runtime);
        director = (TextView) findViewById(R.id.director);
        awardsText = (TextView) findViewById(R.id.awardsText);
        directorText = (TextView) findViewById(R.id.directorText);
        starring = (TextView) findViewById(R.id.starring);
        releaseDateText = (TextView) findViewById(R.id.releaseDateText);
        timer = (LinearLayout) findViewById(R.id.timerDisplay);
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        Intent intent = getIntent();
        finishedMovie = (FinishedMovie) intent.getSerializableExtra("movieDataFinal");
        setData();
        playMovie();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if (fromUser) {
                    // this is when actually seekbar has been seeked to a new position
                    if (mVideoView != null) {
                        mVideoView.seekTo(progress);
                    }
                }
            }
        });

//        play.setImageResource(R.drawable.pausetrending);

        mVideoView.addExoPlayerListener(new ExoPlayerListener() {
            @Override
            public void onStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady) {
                    if (playbackState == 4) {
                        progressBar.setVisibility(View.GONE);
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onVideoSizeChanged(int width, int height, int unAppliedRotationDegrees, float pixelWidthHeightRatio) {

            }
        });

        mVideoView.postDelayed(hideControlsThread, 5000);

        mVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (everyThingHidden)
                    showControls();
                else {
                    hideControls();
                }
            }
        });
        play.setVisibility(View.GONE);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                vibrator.vibrate(20);
                if (mVideoView != null) {
                    if (mVideoView.isPlaying()) {
                        mVideoView.pause();
                        play.setImageResource(R.drawable.ic_play_arrow_white_100dp);
                        progressBar.setVisibility(View.GONE);
                    } else {
                        mVideoView.start();
                        play.setImageResource(R.drawable.ic_pause_white_100dp);
                        mVideoView.postDelayed(hideControlsThread, 5000);
                    }
                    if (isVideoComplete) {
                        if (mVideoView != null) {
                            isVideoComplete = false;
                            mVideoView.seekTo(0);
                            mVideoView.setVideoURI(Uri.parse(finishedMovie.dUrl));
                            mVideoView.start();
                            mVideoView.postDelayed(hideControlsThread, 5000);
                        }
                    }
                }
            }
        });

        fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFullScreenClicked = true;
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });
    }

    private void playMovie() {

        mVideoView.setVideoURI(Uri.parse(finishedMovie.dUrl));
        mVideoView.start();
        progressBar.setVisibility(View.VISIBLE);
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                seekBar.setMax((int) mVideoView.getDuration());
                progressBar.setVisibility(View.GONE);
                play.setImageResource(R.drawable.pausetrending);
                videoDuration.setText(String.format(" %02d:%02d ",
                        TimeUnit.MILLISECONDS.toMinutes(mVideoView.getDuration()),
                        TimeUnit.MILLISECONDS.toSeconds(mVideoView.getDuration()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mVideoView.getDuration()))));
                seekBar.postDelayed(onEverySecond, 1000);

                videoCurrentTime.setText(String.format(" %02d:%02d ",
                        TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()),
                        TimeUnit.MILLISECONDS.toSeconds(mVideoView.getCurrentPosition()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()))));

            }
        });
    }

    private void setData() {
        title.setText(finishedMovie.title);
        actors.setText(finishedMovie.actors);
        plot.setText(finishedMovie.plot);
        releaseDate.setText(finishedMovie.release);
        rating.setText("IMDB: " + finishedMovie.rating);
        awards.setText(finishedMovie.awards);
        genre.setText(finishedMovie.genre);
        runtime.setText(finishedMovie.runtime);
        director.setText(finishedMovie.director);

        if (finishedMovie.awards.equalsIgnoreCase("N/A")) {
            awards.setVisibility(View.GONE);
            awardsText.setVisibility(View.GONE);
        }
        if (finishedMovie.rating.equalsIgnoreCase("N/A")) {
            rating.setVisibility(View.GONE);
            ratingStars.setVisibility(View.GONE);
        }
        if (finishedMovie.release.equalsIgnoreCase("N/A")) {
            releaseDate.setVisibility(View.GONE);
            releaseDateText.setVisibility(View.GONE);
        }
        if (finishedMovie.director.equalsIgnoreCase("N/A")) {
            director.setVisibility(View.GONE);
            directorText.setVisibility(View.GONE);
        }
        if (finishedMovie.actors.equalsIgnoreCase("N/A")) {
            actors.setVisibility(View.GONE);
            starring.setVisibility(View.GONE);
        }

        try {
            Log.d("msg", finishedMovie.rating + "");
            ratingStars.setMax(5);
            ratingStars.setRating(Float.parseFloat(finishedMovie.rating)/2);
        }
        catch (Exception ignored){

        }

        rating.setTypeface(FontUtils.SecfontB);
        releaseDate.setTypeface(FontUtils.SecfontR);
        actors.setTypeface(FontUtils.SecfontR);
        title.setTypeface(FontUtils.SecfontB);
        plot.setTypeface(FontUtils.SecfontR);
        awards.setTypeface(FontUtils.SecfontR);
        genre.setTypeface(FontUtils.SecfontR);
        runtime.setTypeface(FontUtils.SecfontR);
        director.setTypeface(FontUtils.SecfontR);
    }

    private static boolean isVideoComplete;
    private static Runnable onEverySecond = new Runnable() {
        {

        }
        @Override
        public void run() {

            if (seekBar != null) {
                seekBar.setProgress((int) mVideoView.getCurrentPosition());
                videoCurrentTime.setText(String.format(" %02d:%02d ",
                        TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()),
                        TimeUnit.MILLISECONDS.toSeconds(mVideoView.getCurrentPosition()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mVideoView.getCurrentPosition()))));
            }

            if (mVideoView != null) {
                if (mVideoView.isPlaying()) {
                    seekBar.postDelayed(onEverySecond, 1000);
                }
            }

            if (mVideoView != null) {
                if (mVideoView.getCurrentPosition() >= mVideoView.getDuration()) {
                    isVideoComplete = true;
                    progressBar.setVisibility(View.GONE);
                    play.setImageResource(R.drawable.playtrending);
                }
            }
        }
    };

    private Runnable hideControlsThread = new Runnable() {
        @Override
        public void run() {

            hideControls();
        }
    };

    private void hideControls() {

        if (mVideoView.isPlaying()) {
            play.setVisibility(View.GONE);
            everyThingHidden = true;
        }
    }

    private void showControls() {

        play.setVisibility(View.VISIBLE);
        if (mVideoView.isPlaying()) {
            play.setImageResource(R.drawable.ic_pause_white_100dp);
        } else {
            play.setImageResource(R.drawable.ic_play_arrow_white_100dp);
        }

        everyThingHidden = false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(isFullScreenClicked) {
            isFullScreenClicked = false;
            ViewGroup.LayoutParams params = mVideoView.getLayoutParams();
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                DisplayMetrics metrics = new DisplayMetrics();
                display.getMetrics(metrics);
                float width = metrics.widthPixels;
                float height = metrics.heightPixels;
                Log.d("width", "" + width);
                Log.d("height", "" + height);

                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                float pixels = Utils.convertDpToPixel(28.0f, this);
                params.height = (int) height - (int) pixels;
                if (mVideoView != null) {
                    mVideoView.requestLayout();
                }
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200.0f, getResources().getDisplayMetrics());
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = height;
                mVideoView.requestLayout();
            }
        }
        else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isFullScreenClicked = true;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
