package com.hack.lucifer.movies.Movies;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;

public class FinishedMovie implements Serializable, Comparable<FinishedMovie>, Parcelable {

    public String title, year, release, runtime,
            genre, director, actors, plot,
            language, awards, iurl, rating,
            dUrl, size;

    public FinishedMovie() {

    }

    protected FinishedMovie(Parcel in) {
        title = in.readString();
        year = in.readString();
        release = in.readString();
        runtime = in.readString();
        genre = in.readString();
        director = in.readString();
        actors = in.readString();
        plot = in.readString();
        language = in.readString();
        awards = in.readString();
        iurl = in.readString();
        rating = in.readString();
        dUrl = in.readString();
        size = in.readString();
    }

    public static final Creator<FinishedMovie> CREATOR = new Creator<FinishedMovie>() {
        @Override
        public FinishedMovie createFromParcel(Parcel in) {
            return new FinishedMovie(in);
        }

        @Override
        public FinishedMovie[] newArray(int size) {
            return new FinishedMovie[size];
        }
    };

    @Override
    public int compareTo(@NonNull FinishedMovie another) {
        return this.title.compareTo(another.title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(year);
        dest.writeString(release);
        dest.writeString(runtime);
        dest.writeString(genre);
        dest.writeString(director);
        dest.writeString(actors);
        dest.writeString(plot);
        dest.writeString(language);
        dest.writeString(awards);
        dest.writeString(iurl);
        dest.writeString(rating);
        dest.writeString(dUrl);
        dest.writeString(size);
    }
}
