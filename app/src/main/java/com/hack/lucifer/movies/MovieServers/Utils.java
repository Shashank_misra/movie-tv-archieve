package com.hack.lucifer.movies.MovieServers;

import com.hack.lucifer.movies.Movies.FinishedMovie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Utils {

    public static FinishedMovie parseMovieBlob(JSONObject m) throws JSONException {

        FinishedMovie fm = new FinishedMovie();
        fm.title = m.getString("title");
        fm.year = m.getString("year");
        fm.release = m.getString("release");
        fm.runtime = m.getString("runtime");
        fm.size = m.getString("size");
        fm.genre = m.getString("genre");
        fm.director = m.getString("director");
        fm.actors = m.getString("actors");
        fm.plot = m.getString("plot");
        fm.language = m.getString("language");
        fm.dUrl = m.getString("dUrl");
        fm.awards = m.getString("awards");
        fm.iurl = m.getString("iUrl");
        fm.rating = m.getString("rating");

        return fm;
    }

    public static FinishedMovie parseOMDBDataForMovie(String json, String dUrl, String size, String name) {

        FinishedMovie result;
        try {
            result = new FinishedMovie();
            JSONObject object = new JSONObject(json);
            if (object.getString("Response").equals("True")) {
                result.title = object.getString("Title");
                result.year = object.getString("Year");
                result.release = object.getString("Released");
                result.runtime = object.getString("Runtime");
                result.genre = object.getString("Genre");
                result.director = object.getString("Director");
                result.actors = object.getString("Actors");
                result.plot = object.getString("Plot");
                result.language = object.getString("Language");
                result.awards = object.getString("Awards");
                result.iurl = object.getString("Poster");
                result.rating = object.getString("imdbRating");
                result.dUrl = dUrl;
                result.size = size;

//                Log.d("succ parse " + ++parseSucc + ": ", name);
            } else {
                result = null;
//                Log.d("parsing fail " + ++parseFailCount + ": ", name);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result = null;
        }

        return result;
    }

    public static JSONObject getTemplateJsonForMovie(ArrayList<FinishedMovie> globalReady, String[] serverName, String[] serverURL, int position) {

        JSONObject server_1 = new JSONObject();
        JSONArray movieList = new JSONArray();
        try {
            server_1.put("serverName", serverName[0]);
            server_1.put("serverURL", serverURL[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            for (int i = position; i < globalReady.size(); i++) {

                FinishedMovie m = globalReady.get(i);
                JSONObject obj = new JSONObject();
                obj.put("title", m.title);
                obj.put("year", m.year);
                obj.put("release", m.release);
                obj.put("runtime", m.runtime);
                obj.put("size", m.size);
                obj.put("genre", m.genre);
                obj.put("director", m.director);
                obj.put("actors", m.actors);
                obj.put("plot", m.plot);
                obj.put("language", m.language);
                obj.put("dUrl", m.dUrl);
                obj.put("awards", m.awards);
                obj.put("iUrl", m.iurl);
                obj.put("rating", m.rating);

                movieList.put(i, obj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            server_1.put("movieList", movieList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return server_1;
    }

}
