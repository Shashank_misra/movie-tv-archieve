package com.hack.lucifer.movies.DownloadManager;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.HashSet;

public class ReceiverDownloadClicked extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("msg", "in Clicked broadcast");

        DBManager downDBManager = new DBManager(context);
        HashSet<Long> downloadIdSet = downDBManager.getAllDownloadIds();
        String extraId = DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS;
        long[] references = intent.getLongArrayExtra(extraId);

        for (long reference : references) {
            if (downloadIdSet.contains(reference)) {
                Log.d("msg", "match found in notification clicked");
                Intent launchApp = new Intent(context, com.hack.lucifer.movies.DownloadManager.DownloadActivityV2.class);
                launchApp.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(launchApp);
            } else {
                Log.d("msg", "no match found");
            }
        }
    }
}
